<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ItineraryListController;
use App\Http\Controllers\ItineraryDetailController;
use App\Http\Controllers\ItineraryCustomController;
use App\Http\Controllers\PersonalItineraryController;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

if (Session::get('website_language') == null) {
    Session::put('website_language', config('app.locale'));
}

Route::get('/test-gg-map', function () {
    return view('test-google-map');
});

Route::get('/test', function () {
    return view('welcome');
});

Route::get('/test-sortable', function () {
    return view('test-sortable');
});

Route::group(['middleware' => 'locale'], function () {
    Route::get('/destinations', [BlogController::class, 'getDestinations'])->name('get-destinations');
    Route::get('/gastronomies', [BlogController::class, 'getGastronomies'])->name('get-gastronomies');
    Route::get('/destination/{slug}', [BlogController::class, 'destinationDetail'])->name('destination-detail');
    Route::get('/gastronomy/{slug}', [BlogController::class, 'gastronomyDetail'])->name('gastronomy-detail');

    Route::get('/', [HomepageController::class, 'getIndex'])->name('home');
    Route::get('change-language/{language}', [HomepageController::class, 'changeLanguage'])->name('change-language');
    Route::post('/save-info', [HomepageController::class, 'getSaveInfo'])->name('save-info');
    Route::get('/itinerary-list', [ItineraryListController::class, 'getIndex'])->name('itinerary-list');
    Route::post('/itinerary-list/search', [ItineraryListController::class, 'searchItinerary'])->name('search-itinerary');
    Route::get('/itinerary-detail', [ItineraryDetailController::class, 'getIndex'])->name('itinerary-detail');
    Route::get('/login', [LoginController::class, 'getIndex'])->name('login');
    Route::post('/login/check-account', [LoginController::class, 'postLogin'])->name('check-account');

    Route::middleware(['checkLogin'])->group(function () {
        Route::get('/itinerary-custom', [ItineraryCustomController::class, 'getIndex']);
        Route::post('/itinerary-custom/search', [ItineraryCustomController::class, 'postSearchAttraction']);
        Route::post('/itinerary-custom/searchByTag', [ItineraryCustomController::class, 'postSearchAttractionByTag']);
        Route::post('/itinerary-custom/save', [ItineraryCustomController::class, 'postSave']);

        Route::get('/itinerary-create', [AdminController::class, 'newTour'])->name('itinerary-create');

        Route::get('/personal-itinerary', [PersonalItineraryController::class, 'getIndex'])->name('personal-itinerary');
        Route::get('/logout', [PersonalItineraryController::class, 'logout']);

        Route::get('/load-blade/{target}', [AdminController::class, 'loadBlade'])->name('load-blade');

        Route::get('/import-destination', [AdminController::class, 'importDestination'])->name('import-destination');
        Route::post('/upload-destination', [AdminController::class, 'uploadDestination'])->name('upload-destination');

        Route::get('/update-destination/{id}', [AdminController::class, 'updateDestination'])->name('update-destination');
        Route::get('/update-destination', [AdminController::class, 'updateDestination'])->name('update-destination');
        Route::get('/delete-destination/{id}', [AdminController::class, 'deleteDestination'])->name('delete-destination');
        Route::post('update-destination/save', [AdminController::class, 'SaveAttraction'])->name('save-attraction');
        Route::get('blog/{id}', [AdminController::class, 'getBlog'])->name('get-blog');
        Route::post('blog/save', [AdminController::class, 'saveBlog'])->name('save-blog');

        Route::get('/update-tag/{id}', [AdminController::class, 'updateTag'])->name('update-tag');
        Route::get('update-tag', [AdminController::class, 'updateTag'])->name('update-tag');
        Route::post('update-tag/save', [AdminController::class, 'saveTag'])->name('save-tag');
        Route::get('/delete-tag/{id}', [AdminController::class, 'deleteTag'])->name('delete-tag');

        Route::get('/update-tour/{id}', [AdminController::class, 'updateTour'])->name('update-tour');
        Route::get('update-tour', [AdminController::class, 'updateTour'])->name('update-tour');
        // Route::post('update-tour/save', [AdminController::class, 'saveTour'])->name('save-tour');
        Route::get('/delete-tour/{id}', [AdminController::class, 'deleteTour'])->name('delete-tour');
        Route::post('/load-tour-detail', [AdminController::class, 'loadTourDetail'])->name('load-tour-detail');
        Route::post('/update-tour', [AdminController::class, 'saveTour'])->name('post-update-tour');

        Route::get('/update-post/{id}', [AdminController::class, 'updatePost'])->name('update-post');
        Route::get('update-post', [AdminController::class, 'updatePost'])->name('update-post');
        Route::post('update-post/save', [AdminController::class, 'savePost'])->name('save-post');
        Route::get('/delete-post/{id}', [AdminController::class, 'deletePost'])->name('delete-post');

        Route::get('/update-user/{id}', [AdminController::class, 'updateUser'])->name('update-user');
        Route::get('update-user', [AdminController::class, 'updateUser'])->name('update-user');
        Route::post('update-user/save', [AdminController::class, 'saveUser'])->name('save-user');
        Route::get('/delete-user/{id}', [AdminController::class, 'deleteUser'])->name('delete-user');

    });
});
