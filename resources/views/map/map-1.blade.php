<script >
    var map1, map2, attraction_1 = [], markers_1 = [];
    var latlngbounds_1;
    var bounds;
    var attractions = {!! json_encode($attractions) !!};
    // set data list data to display markers
    for(var i = 0; i < attractions.length; i++){
        if(attractions[i]['day'] === '1'){
            attraction_1.push({lat : parseFloat(attractions[i]['lat']), lng : parseFloat(attractions[i]['long'])})
        }
    }
    // init maps
    function initMap() {
        map1 = new google.maps.Map(document.getElementById('item-tour-map-1'), {
            center: {lat: 16.054407, lng: 108.202164},
            zoom: 15
        });

        latlngbounds_1 = new google.maps.LatLngBounds();
        bounds = new google.maps.LatLngBounds();
    }
    setTimeout(addMarkers, 1000);
    // add markers to maps
    function addMarkers() {
        for (var x = 0; x < attraction_1.length; x++) {
            addMarker(map1, markers_1, latlngbounds_1,attraction_1[x]);
        }
        map1.setCenter(latlngbounds_1.getCenter());
        map1.fitBounds(latlngbounds_1);
    }
    // add marker to map
    function addMarker(p_map,p_marker,p_latlngbounds,position) {
        var x = new google.maps.Marker({
            position: position,
            map: p_map,
        });
        p_marker.push(x);
        p_latlngbounds.extend(x.position);
    }
</script>


