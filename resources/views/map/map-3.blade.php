<script >
    var map1, map2, map3, attraction_1 = [], attraction_2 = [], attraction_3 = [], markers_1 = [], markers_2 = [], markers_3 = [];
    var latlngbounds_1, latlngbounds_2, latlngbounds_3;
    var bounds;
    var attractions = {!! json_encode($attractions) !!};
    // set data list data to display markers
    for(var i = 0; i < attractions.length; i++){
        if(attractions[i]['day'] === '1'){
            attraction_1.push({lat : parseFloat(attractions[i]['lat']), lng : parseFloat(attractions[i]['long'])})
        }
        if(attractions[i]['day'] === '2'){
            attraction_2.push({lat :parseFloat(attractions[i]['lat']), lng : parseFloat(attractions[i]['long'])})
        }
        if(attractions[i]['day'] === '3'){
            attraction_3.push({lat :parseFloat(attractions[i]['lat']), lng : parseFloat(attractions[i]['long'])})
        }
    }
    // init maps
    function initMap() {
        map1 = new google.maps.Map(document.getElementById('item-tour-map-1'), {
            center: {lat: 16.054407, lng: 108.202164},
            zoom: 15
        });
        map2 = new google.maps.Map(document.getElementById('item-tour-map-2'), {
            center: {lat: 16.054407, lng: 108.202164},
            zoom: 15
        });
        map2 = new google.maps.Map(document.getElementById('item-tour-map-3'), {
            center: {lat: 16.054407, lng: 108.202164},
            zoom: 15
        });
        latlngbounds_1 = new google.maps.LatLngBounds();
        latlngbounds_2 = new google.maps.LatLngBounds();
        latlngbounds_3 = new google.maps.LatLngBounds();
        bounds = new google.maps.LatLngBounds();
    }
    setTimeout(addMarkers, 1000);
    // add markers to maps
    function addMarkers() {
        for (var i = 0; i < 2; i++) {
            if( (i+1) === 1){
                for (var x = 0; x < attraction_1.length; x++) {
                    addMarker(map1, markers_1, latlngbounds_1,attraction_1[x]);
                }
                map1.setCenter(latlngbounds_1.getCenter());
                map1.fitBounds(latlngbounds_1);
            }
            if( (i+1) === 2){
                for (var x = 0; x < attraction_2.length; x++) {
                    addMarker(map2, markers_2, latlngbounds_2,attraction_2[x]);
                }
                map2.setCenter(latlngbounds_2.getCenter());
                map2.fitBounds(latlngbounds_2);
            }
            if( (i+1) === 3){
                for (var x = 0; x < attraction_3.length; x++) {
                    addMarker(map3, markers_3, latlngbounds_3, attraction_3[x]);
                }
                map3.setCenter(latlngbounds_3.getCenter());
                map3.fitBounds(latlngbounds_3);
            }
        }

    }
    // add marker to map
    function addMarker(p_map,p_marker,p_latlngbounds,position) {
        var x = new google.maps.Marker({
            position: position,
            map: p_map,
        });
        p_marker.push(x);
        p_latlngbounds.extend(x.position);
    }
</script>


