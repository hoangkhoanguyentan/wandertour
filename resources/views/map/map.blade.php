<script >
    var directionsService;
    // array contains route each map
    var directionsDisplays = [];
    //
    var distanceService;
    // array contains maps
    var maps            = [];
    // array contains attractions each day
    var dateAttactions  = [];
    // array contain markers
    var markers         = [];
    // array contains object latlng
    var latlngbounds    = [];
    // get all attraction of tour
    var attractions = {!! json_encode($attractions) !!};
    var duration    = {!! json_encode($duration) !!};
    //
    for (var i = 0; i < duration; i++) {
        var arr = [];
        dateAttactions.push(arr);
        markers.push([]);
        for (var j = 0; j < attractions.length; j++) {
            if(attractions[j]['day'] === ''+(i+1)){
                dateAttactions[i].push({
                    lat    : parseFloat(attractions[j]['lat']),
                    lng    : parseFloat(attractions[j]['long']),
                    title  : attractions[j]['attr_name'],
                    address: attractions[j]['attr_address']});
            }
        }
    }
    // init maps
    function initMap() {
        // init map
        for (var i = 0; i < duration; i++) {
            maps.push(new google.maps.Map(document.getElementById('item-tour-map-'+(i+1)), {
                center : {lat: 16.054407, lng: 108.202164},
                zoom   : 15
            }));
            latlngbounds.push(new google.maps.LatLngBounds());
        }
        // draw route
        directionsService = new google.maps.DirectionsService;
        for (var i = 0; i < duration; i++) {
            directionsDisplays.push(new google.maps.DirectionsRenderer({map: maps[i],  suppressMarkers: true}));
            calculateAndDisplayRoute(directionsService, directionsDisplays[i], dateAttactions[i]);
        }
        distanceService   = new google.maps.DistanceMatrixService;
        // add markers to view
        addMarkers();
        // set time and distance to view
        for (var i = 0; i < duration; i++) {
            setTimeAndDistance(dateAttactions[i],distanceService, i+1);
        }
    }
    // add markers to maps
    function addMarkers() {
        for (var i = 0; i < duration; i++) {
            for (var x = 0; x < dateAttactions[i].length; x++) {
                addMarker(maps[i],markers[i], latlngbounds[i],dateAttactions[i][x], x+1);
            }
            maps[i].setCenter(latlngbounds[i].getCenter());
            maps[i].fitBounds(latlngbounds[i]);
        }
    }
    // add marker to map
    function addMarker(p_map,p_marker,p_latlngbounds,attraction, no) {
        var latLong     = new google.maps.LatLng(attraction.lat, attraction.lng);
        var infoWindow  = new google.maps.InfoWindow();
        var marker      = new google.maps.Marker({
            position : latLong,
            map      : p_map,
            title    : attraction.title,
            label    : '' + no
        });
        infoWindow.setContent(
            "<div style='font-size: 16px; font-weight:bold'>"+ attraction.title+"</div>"
            +"<div style = 'width:200px;min-height:40px'>" + attraction.address + "</div>");
        marker.addListener('click', function() {
            infoWindow.open(p_map, marker);
        });
        p_marker.push(marker);
        p_latlngbounds.extend(marker.position);
    }
    // draw route
    function calculateAndDisplayRoute(directionsService, directionsDisplay, markerArray) {

        var waypts = [];
        for (var i = 0; i < markerArray.length; i++) {
            if ((i !== 0) && ( i !== markerArray.length)) {
                waypts.push({
                    location : new google.maps.LatLng(markerArray[i].lat, markerArray[i].lng),
                    stopover : true
                });
            }
        }
        directionsService.route({
            origin      : new google.maps.LatLng(markerArray[0].lat, markerArray[0].lng),
            destination : new google.maps.LatLng(markerArray[markerArray.length-1].lat, markerArray[markerArray.length-1].lng),
            waypoints   : waypts,
            optimizeWaypoints : true,
            travelMode  : 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response;
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

    }
    // set time and distance for view
    function setTimeAndDistance(arrayAttraction,distanceService, day) {
        for (var i = 0; i < arrayAttraction.length-1; i++) {

            initDistance(
                distanceService,
                new google.maps.LatLng(arrayAttraction[i].lat, arrayAttraction[i].lng),
                new google.maps.LatLng(arrayAttraction[i+1].lat, arrayAttraction[i+1].lng),
                i,
                function (response, no) {
                    var distance = response.rows[0].elements[0].distance;
                    var duration = response.rows[0].elements[0].duration;
                    // console.log(distance.text, duration.text);
                    document.getElementById("route-km-" + day + "-" + (no+1)).innerText = distance.text;
                    document.getElementById("route-time-" + day + "-" + (no+1)).innerText = duration.text;
                }
            );

        }
    }
    // calculate distance and time
    function initDistance(distanceService, origins, destination, no, callback) {
        distanceService.getDistanceMatrix(
            {
                origins: [origins],
                destinations: [destination],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false,
            }, function (response, status) {
                if (status !== 'OK') {
                    alert('Error was: ' + status);
                } else {
                    callback(response, no);
                }
            });
    }
</script>


