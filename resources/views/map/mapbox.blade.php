<script>
    var attractions = {!! json_encode($attractions) !!};
    var duration = {!! json_encode($duration) !!};
    var dateAttactions = [];

    $(document).ready(function() {
        // array contains maps
        // array contain markers
        for (var i = 0; i < duration; i++) {
            var arr = [];
            dateAttactions.push(arr);
            for (var j = 0; j < attractions.length; j++) {
                if (attractions[j]['day'] == '' + (i + 1)) {
                    dateAttactions[i].push({
                        lat: parseFloat(attractions[j]['lat']),
                        lng: parseFloat(attractions[j]['long']),
                        title: attractions[j]['attr_name'],
                        address: attractions[j]['attr_address'],
                        day: attractions[j]['day']
                    });
                }
            }
        }

        initMap();
    })

    // init maps
    function initMap() {
        console.log("duration: ", duration);
        console.log("dateAttactions: ", dateAttactions);
        var mapsByDay = {};
        for (var i = 1; i < duration; i++) {
            if (!mapsByDay[i]) {
                // Nếu chưa có bản đồ cho ngày này, tạo một bản đồ mới
                mapsByDay[i] = new mapboxgl.Map({
                    container: 'item-tour-map-' + i, // Sử dụng container tương ứng với ngày
                    style: 'mapbox://styles/mapbox/streets-v12',
                    center: [108.202167, 16.054407],
                    zoom: 12
                });
            }

            // Tạo một marker cho địa điểm và thêm vào bản đồ của ngày đó
            var currentData = dateAttactions[i - 1];
            console.log('Day: ', i);
            console.log('Data', currentData);
            if (currentData) {
                currentData.forEach(element => {
                    new mapboxgl.Marker()
                        .setLngLat([element.lng, element.lat])
                        .setPopup(new mapboxgl.Popup().setHTML(element.title))
                        .addTo(mapsByDay[i]);
                });
            }

        }
    }
</script>
