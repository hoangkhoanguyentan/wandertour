@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/blog.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
    <script type="text/javascript" src="{{ asset('js/app/itinerary-detail.js') }}"></script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container gastronomy-content">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ trans('home.Home') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info"
                            href="/gastronomies">{{ trans('home.Gastronomies') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info" href="#">
                            @if (Session::get('website_language', config('app.locale')) == 'vi')
                                {{ $blog->title }}
                            @else
                                {{ $blog->en_title }}
                            @endif
                        </a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12 no-padding-left no-padding-right" id="tour_detail_info">
            <div id="tour-detail-info-top" style="height: auto; padding: 0px 10px;">
                @yield('blog-detail')
            </div>
            {{-- <div class="col-md-5">
            <div id="tour-qc" class="sidebar tour-qc-box">
                <div class="sidebar-widget">
                    <h2 class="widget-title">Bài viết mới nhất</h2>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Keeping cooking simple</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/food.jpg"
                            alt="two dumplings on a wood plate with chopsticks" class="widget-image">
                    </div>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Simplicity and work</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/work.jpg"
                            alt="a chair white chair at a white desk on a white wall" class="widget-image">
                    </div>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Simple decorations</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/deco.jpg"
                            alt="a green plant in a clear, round vase with water in it" class="widget-image">
                    </div>
                </div>

            </div>
        </div> --}}
        </div>
    </div>

    {{-- <div class="container container-flex container-blog">
        <main role="main">

        </main>

        <aside class="sidebar">

        </aside>
    </div> --}}
@endsection
