@extends('master')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/index.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-item.css') }}">
    <script type="text/javascript" src="{{ asset('js/app/index.js') }}"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/library/bootstrap-select.css') }}">

    <!-- Latest compiled and minified JavaScript -->
    <script src="{{ asset('js/library/bootstrap-select.js') }}"></script>
    <script>
        var lang = {!! json_encode(Session::get('website_language')) !!};
    </script>
@endsection
@section('content')
    <header
        <nav>
            <div class="row">
                <img src="{{ asset('image/danang.png') }}" alt="Danang travel" class="logo">
                <img src="{{ asset('image/danang.png') }}" alt="Danang travel" class="logo-black">
                <ul class="main-nav js--main-nav">
                    <li><a href="#destinations">{{ trans('home.Destination') }}</a></li>
                    <li><a href="#gastronomy">{{ trans('home.Gastronomies') }}</a></li>
                    <li><a href="#itinerary">{{ trans('home.Itinerary') }}</a></li>

                    <?php
                    echo '<li>';
                    if (session()->has('userInfo')) {
                        echo '<a href="/personal-itinerary">' . trans('home.Account') . '</a>';
                    } else {
                        echo '<a href="/login">' . trans('home.Login') . '</a>';
                    }
                    echo '</li>';
                    if (session()->has('userInfo')) {
                        echo '<li>';
                        echo '<a href="/logout">' . trans('home.Logout') . '</a>';
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
        </nav>
        <div class="hero-text-box">
            <h1>{{ trans('home.Plan and go') }}</h1>
            <p>{{ trans('home.We make planning your trip easier') }}</p>
        </div>
        <div class="plan-text-box">
            <div class="row">
                <div class="col-sm-2 col-md-2 duration-div no-padding">

                </div>
                <div class="col-sm-2 col-md-2 duration-div no-padding">
                    <!-- <label>Thời gian</label> -->
                    <select id="duration" title="{{ trans('home.ChooseTime') }}">
                        <option value="1">1 {{ trans('home.days') }}</option>
                        <option value="2">2 {{ trans('home.days') }}</option>
                        <option value="3">3 {{ trans('home.days') }}</option>
                        <option value="4">4 {{ trans('home.days') }}</option>
                        <option value="5">5 {{ trans('home.days') }}</option>
                    </select>
                </div>
                <div class="col-sm-9 col-md-4 interested-div no-padding">
                    <!-- <label>Sở thích</label> -->
                    <select id="interested" multiple="multiple" data-selected-text-format="count > 3"
                        title="{{ trans('home.ChooseInterests') }}">
                        @forelse($data['interested'] as $item)
                            @if (Session::get('website_language', config('app.locale')) == 'vi')
                                <option data-content="<span class='badge'>{{ $item->name }}</span>"
                                    value="{{ $item->name }}">{{ $item->name }}</option>
                            @else
                                <option data-content="<span class='badge'>{{ $item->en_name }}</span>"
                                    value="{{ $item->en_name }}">{{ $item->en_name }}</option>
                            @endif
                        @empty
                            <p>There are no interests yet!</p>
                        @endforelse
                    </select>
                </div>
                <div class="col-sm-9 col-md-3  no-padding btn-plan">
                    <a href="#" id="btn-submit" class="planning"><span>{{ trans('home.FindItinerary') }}</span></a>
                </div>
            </div>
        </div>
    </header>
    <section class="section-work">
        <div class="row">
            <div class="col-md-4 col-sm-6 content-howitwork">
                <div class="icon-section-work">
                    <img src="{{ asset('image/howitwork_1.png') }}">
                </div>
                <h3 class="title-section-work">
                    <span>1</span>
                    {{ trans('home.Preferences') }}
                </h3>
                <div class="content-section-work">
                    {{ trans('home.Select your preferred itineraries.') }}
                </div>
            </div>
            <div class="col-md-4 col-sm-6 content-howitwork">
                <div class="icon-section-work">
                    <img src="{{ asset('image/howitwork_2.png') }}">
                </div>
                <h3 class="title-section-work">
                    <span>2</span>
                    {{ trans('home.CustomizeDestinations') }}
                </h3>
                <div class="content-section-work">
                    {{ trans('home.Customize your chosen destinations or create your own.') }}
                </div>
            </div>
            <div class="col-md-4 col-sm-6 content-howitwork">
                <div class="icon-section-work">
                    <img src="{{ asset('image/howitwork_3.png') }}">
                </div>
                <h3 class="title-section-work">
                    <span>3</span>
                    {{ trans('home.EnjoyTrip') }}
                </h3>
                <div class="content-section-work">
                    {{ trans('home.Enjoy your trip to the fullest by customizing itineraries or creating your own.') }}
                </div>
            </div>
        </div>
    </section>
    <section id="destinations" class="section-top-destination">
        <div class="row">
            <h2>
                {{ trans('home.AttractiveDestinations') }}
            </h2>
        </div>
        <div class="row grid-destination">
            @php
                $itemCount = count($data['attraction']);
                $colCount = min($itemCount, 10); // Giới hạn tối đa 10 mục
            @endphp

            @for ($i = 0; $i < $colCount; $i++)
                @php
                    $columnClass = 'col-lg-4';
                    if ($i == 0) {
                        $columnClass = 'col-lg-8';
                    } elseif ($i == 9) {
                        $columnClass = 'col-lg-8';
                    } else {
                        $columnClass = 'col-lg-4';
                    }
                @endphp

                <div class="{{ $columnClass }} col-md-4 col-sm-6 no-padding-right">
                    <div class="grid-item col-small">
                        <a href="{{ 'destination/' . $data['attraction'][$i]->blog_slug }}">
                            <img src="{{ asset('image/' . $data['attraction'][$i]->image_link) }}" class="img-center">
                        </a>
                        <div class="grid-intro">
                            @if (Session::get('website_language') == 'en')
                                <p>{{ $data['attraction'][$i]->en_attr_name }}</p>
                            @else
                                <p>{{ $data['attraction'][$i]->attr_name }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </section>
    <section id="gastronomy" class="section-top-destination">
        <div class="row">
            <h2>
                {{ trans('home.LocalCuisine') }}
            </h2>
        </div>
        <div class="row grid-destination">
            @php
                $itemCount = count($data['blog']);
                $colCount = min($itemCount, 10); // Giới hạn tối đa 10 mục
            @endphp

            @for ($i = 0; $i < $colCount; $i++)
                @php
                    $columnClass = 'col-lg-4';
                    if ($i == 0) {
                        $columnClass = 'col-lg-8';
                    } elseif ($i == 9) {
                        $columnClass = 'col-lg-8';
                    } else {
                        $columnClass = 'col-lg-4';
                    }
                @endphp

                <div class="{{ $columnClass }} col-md-4 col-sm-6 no-padding-right">
                    <div class="grid-item col-small">
                        <a href="{{ 'gastronomy/' . $data['blog'][$i]->blog_slug }}">
                            <img src="{{ asset('image/' . $data['blog'][$i]->image_link) }}" class="img-center">
                        </a>
                        <div class="grid-intro">
                            @if (Session::get('website_language') == 'en')
                                <p>{{ $data['blog'][$i]->en_name }}</p>
                            @else
                                <p>{{ $data['blog'][$i]->name }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </section>
    <section id="itinerary" class="section-top-itinerary">
        <div class="row">
            <h2>
                {{ trans('home.RecommendedItinerary') }}
            </h2>
        </div>
        <div class="row grid-recommended">
            @forelse($data['tour'] as $item)
                <div class="col-sm-6 col-md-3 col-lg-3 no-padding-right">
                    <div class="thumbnail-grid thumbnail ">
                        <div class="thumbnail-img thumbscrubber">
                            <a href="#">
                                <img src="{{ asset('image/' . $item->link) }}">
                            </a>
                        </div>
                        <div class="thumbnail-body">
                            <div class="caption">
                                <a href="#" class="prop-title">
                                    {{ $item->tour_name }}
                                </a>
                                {{-- <div class="rating col-xs-5">
                                    <span class="sprite star-ora"></span>
                                    <span class="sprite star-ora"></span>
                                    <span class="sprite star-ora"></span>
                                    <span class="sprite star-ora"></span>
                                    <span class="sprite star"></span>
                                </div> --}}
                                {{-- <div class="tl-right col-nopadding col-xs-7 pull-right">
                                    <span class="sprite eye"></span>353
                                    <span class="sprite download"></span>2
                                </div> --}}
                            </div>
                            <div class="thumbnail-content clearfix">
                                <ul class="list-unstyled feature-list">
                                    <li>
                                        <span>{{ trans('home.Amount') }}</span>
                                        <label class="pull-right">
                                            {{ $item->tour_budget = 0 ? trans('itinerary.free') : $item->tour_budget }}&nbsp;
                                            <small>VND</small>
                                        </label>
                                    </li>
                                    <li>
                                        <span>{{ trans('home.Duration') }}</span>
                                        <label class="pull-right">
                                            {{ $item->tour_duration }}&nbsp;
                                            <small>{{ trans('home.days') }}</small>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="link-action clearfix">
                                <a href="#" title="" class="col-xs-6 text-center tipB"
                                    tour-id="{{ $item->tour_id }}">{{ trans('home.ViewItinerary') }}</a>

                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>
                    {{ trans('home.NoRecommendedItinerary') }}
                </p>
            @endforelse
        </div>
    </section>
@endsection
