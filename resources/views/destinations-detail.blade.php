@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/blog.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container desination-content">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ trans('home.Home') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info"
                            href="/destinations">{{ trans('home.Destination') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info" href="#">
                        @if (Session::get('website_language', config('app.locale')) == 'vi')
                            {{ $blog->title }}
                        @else
                            {{ $blog->en_title }}
                        @endif
                    </a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-md-12 no-padding-left no-padding-right" id="tour_detail_info">
            <div id="tour-detail-info-top" style="height: auto; padding: 0px 10px;">
                @yield('blog-detail')
            </div>
        </div>
    </div>
@endsection
