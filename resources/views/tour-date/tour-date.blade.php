<div class="row" id="section-{{$i}}" style="height: auto">
    <div class="tour-date">
        <div class="tour-date-item" id="tour-date-item-{{$i}}">
            <div class="item-title">
                <ion-icon name="calendar" style="line-height: 2px"></ion-icon><span> {{ trans('itinerary.Day') }} {{$i}}</span>
                    {{--<a href="#collapse-{{$i}}"  data-toggle="collapse">a</a>--}}
                <ion-icon name="arrow-dropleft-circle" class="fright btn-collapse" direction="left" id="btn-collapse-left-{{$i}}" href="#collapse-{{$i}}" data-toggle="collapse"></ion-icon>
                <ion-icon name="arrow-dropleft-circle" style="display: none" direction="down" class="fright btn-collapse" id="btn-collapse-right-{{$i}}" href="#collapse-{{$i}}" data-toggle="collapse"></ion-icon>
                <span class="badge number-place fright">{{$numberAttrEachDay[$i-1]}} {{ trans('itinerary.destination') }}</span>

            </div>


            <div id="collapse-{{$i}}" class="collapse">
                <div class="item-tour-info">
                    <ul class="timeline" id="info-route-{{$i}}">
                        @for ($y= 0; $y < count($data['tour_detail']); $y++)
                            @if(($i) == $data['tour_detail'][$y]->day)
                                <li>
                                    @include('tour-date.timeline', [
                                        'itinerary' => $tour_detail[$y],
                                        'image'     => $image])
                                </li>
                            @endif
                        @endfor
                    </ul>
                </div>
            </div>
            <div id="item-tour-map-{{$i}}" class="item-map" style="position: relative; overflow: hidden;">
            </div>
        </div>
    </div>
</div>

