<div class="timeline-time">
    <span class="time">{{$itinerary->time}}</span>
</div>
<div class="timeline-icon">
    <a class="place-forcus-map" id="place_0_0" data-lat-lng="20.093377,106.078856" data-day="number-1">
        <ion-icon name="camera"></ion-icon>
    </a>
</div>
<div class="timeline-body">
    <div class="timeline-header">
        <span class="placeimage">
            @for ($i= 0; $i < count($image); $i++)
                @if($itinerary->attr_id == $image[$i]->attr_id)
                    <img class="place" src="{{asset('image/'.$image[$i]->link)}}">
                    @break
                @endif
            @endfor
        </span>
        <span class="placename">{{$itinerary->attr_name}}</span>
        <span class="placeprice pull-right text-muted">{{$itinerary->price}}</span>
    </div>
    <div class="timeline-content">
        <h5 class="template-title">
            <ion-icon name="pin"></ion-icon>
            {{$itinerary->attr_address}}
        </h5>
        {{--<div class="content-info-place">...</div>--}}
        <div class="small_images hidden-xs hidden-sm">
            {{--<a href="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem\Nha%20Tho%20Da\636574853449855511_1.jpg" class="link_image pull-left">--}}
                {{--<img class="" src="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem/Nha%20Tho%20Da/thumb/636574853449855511_1.jpg">--}}
            {{--</a>--}}
            {{--<a href="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem\Nha%20Tho%20Da\636574853449855511_1.jpg" class="link_image pull-left">--}}
                {{--<img class="" src="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem/Nha%20Tho%20Da/thumb/636574853449855511_1.jpg">--}}
            {{--</a>--}}
            {{--<a href="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem\Nha%20Tho%20Da\636574853449855511_1.jpg" class="link_image pull-left">--}}
                {{--<img class="" src="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem/Nha%20Tho%20Da/thumb/636574853449855511_1.jpg">--}}
            {{--</a>--}}
            @for ($i= 0; $i < count($image); $i++)
                @if($itinerary->attr_id == $image[$i]->attr_id)
                    <a class="link_image pull-left">
                        <img class="" src="{{asset('image/'.$image[$i]->link)}}">
                    </a>
                @endif
            @endfor
            <div class="clearfix"></div>
        </div>
    </div>
</div>


