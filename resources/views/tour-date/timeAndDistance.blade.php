<div class="calculater-route-info" id="calculater-{{$day}}-{{$number}}">
    <div class="col-md-6 col-xs-6 col-sm-6">
        <ion-icon name="car"></ion-icon>
        <span class="style-route-car">Ô tô</span>
    </div>
    <div class="col-md-5 col-xs-5 col-sm-5">
        <ion-icon name="arrow-round-down"></ion-icon>
        <span class="style-route-km" id="route-km-{{$day}}-{{$number}}">25,3 km</span>
    </div>
    <div class="col-md-5 col-xs-5 col-sm-5">
        <ion-icon name="clock"></ion-icon>
        <span class="style-route-time" id="route-time-{{$day}}-{{$number}}">36 phút</span>
    </div>
</div>