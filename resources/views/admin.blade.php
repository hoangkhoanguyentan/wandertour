@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-item.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/custom-checkbox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/import.css') }}">
    <script type="text/javascript" src="{{ asset('js/app/itinerary-list.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app/admin.js') }}"></script>

    <link href="{{ asset('css/library/select2.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="{{ asset('css/library/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/library/perfect-scrollbar.css') }}" rel="stylesheet" />

    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
    <script src="{{ asset('js/library/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/mustacle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/perfect-scrollbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/sortable.js') }}"></script>
    <script>
        var selectedMenu = {!! json_encode($data['selectedMenu']) !!};
    </script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                    <li class="breadcrumb-item active"><a class="text-info" href="">Quản trị</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="filters">
            <div class="col-md-4 col-sm-16 no-padding">
                <div class="sidebar-scroll">
                    <div class="sidebar admin">
                        <ul>
                            <li><a href="#" class="menu-item active" data-target="destination">Quản
                                    lý địa điểm</a></li>
                            <li><a href="#" class="menu-item" data-target="tag">Quản lý nhãn</a></li>
                            <li><a href="#" class="menu-item" data-target="tour">Quản lý tour</a></li>
                            <li><a href="#" class="menu-item" data-target="post">Quản lý bài viết</a>
                            </li>
                            <li><a href="#" class="menu-item" data-target="user">Quản lý người dùng</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-16 row">
                <div id="blade-container"></div>
            </div>
        </div>
    </div>
@endsection
