<div class="tour-item-edit tour-item-edit-header mt-10">
    <div class="tour-item-timeline" id="edit-tour-{{$i}}" date-no="{{$i}}">
        <div class="tour-item-timeline-header">
            <ion-icon name="calendar" class="tour-item-timeline-icon"></ion-icon>
            <div class="tour-item-timeline-title">Ngày {{$i}}</div>
        </div>
        <div class="tour-item-timeline-body">
            <ul class="lst-date-item" id="lst_place_date_{{$i}}" date-no="{{$i}}">
                @for ($y= 0; $y < count($data['tour_detail']); $y++)
                    @if(($i) == $data['tour_detail'][$y]->day)
                        <li class="li-parent" data-time-open="{{$tour_detail[$y]->opening_time_start}}"
                        data-time-close="{{$tour_detail[$y]->opening_time_end}}" data-geo="{{$tour_detail[$y]->lat.','.$tour_detail[$y]->long}}"
                        data-id="{{$tour_detail[$y]->attr_id}}" data-visit-time="{{$tour_detail[$y]->attr_duration}}">
                            @include('tour-custom.timeline', [
                                'itinerary' => $tour_detail[$y]])
                        </li>
                    @endif
                @endfor
            </ul>
        </div>
    </div>

</div>