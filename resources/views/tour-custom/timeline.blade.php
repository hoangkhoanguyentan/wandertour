<div class="place-tour-img">
    <img src="{{asset('image/'.$itinerary->link)}}">
</div>
<div class="place-tour-info">
    <p class="name mg-b-0">{{$itinerary->attr_name}}</p>
    <div class="place-tour-info-time">
        <div class="time_o">
            <div>
                <ion-icon name="time" class="fleft"></ion-icon>
                <p class="fleft ">{{ trans('itinerary.open') }}: <span class="time">{{$itinerary->opening_time_start}}</span> </p>
            </div>
            <div class="clearfix"></div>
            <div >
                <ion-icon name="time" class="fleft"></ion-icon>
                <p class="fleft">{{ trans('itinerary.close') }}:  <span class="time">{{$itinerary->opening_time_end}}</span></p>
            </div>
            <div class="clearfix"></div>
        </div>
        <span class="time change-time">{{$itinerary->time}}</span>
        <span class="time-error"></span>
    </div>
    <div class="place-tour-action remove-place"><ion-icon name="close"></ion-icon></div>
</div>