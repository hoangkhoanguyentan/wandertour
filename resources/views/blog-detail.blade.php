@extends('destinations-detail')

@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/blog.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/blog_detail.css') }}">
@endsection

@section('blog-detail')
    <div class="blog-header">
        <h1>
            @if (Session::get('website_language', config('app.locale')) == 'en')
                {{ $blog->en_title }}
            @else
                {{ $blog->title }}
            @endif
        </h1>
        <div>
            <p class="last_update">{{ trans('blog.Lastupdate') }}: {{ substr($blog->updated_at, 0, 10) }}</p>
        </div>
        </article>
        @if (Session::get('website_language', config('app.locale')) == 'en')
            {!! $blog->en_content !!}
        @else
            {!! $blog->content !!}
        @endif

    </div>
@endsection
