@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
    <script type="text/javascript" src="{{ asset('js/app/itinerary-detail.js') }}"></script>

    <link href="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.css" rel="stylesheet">
    <script src="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.js"></script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ trans('itinerary.home') }}</a></li>
                    <li class="breadcrumb-item"><a class="text-info"
                            href="/personal-itinerary?menu=tour">{{ trans('itinerary.itinerary') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info"
                            href="#">{{ trans('itinerary.detail') }}</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-11 no-padding-left no-padding-right" id="tour_detail_info">
            <div id="tour-detail-info-top" style="height: auto">
                <div class="top-panel col-md-16">
                    <h3 class="tour-name" tour-id="{{ $data['tour_info']->tour_id }}">{{ $data['tour_info']->tour_name }}
                    </h3>
                    <div class="col-sm-6 col-md-6 img-tour">
                        <img class="img-responsive" src="{{ asset('image/' . $data['tour_info']->link) }}">
                    </div>
                    <div class="col-sm-10 col-md-10 info-tour">
                        <ul class="lst-info-tour list-unstyled">
                            <li>
                                <span class="fleft">{{ trans('itinerary.from') }}</span>
                                <span class="fright"><b>{{ $data['tour_info']->start_date ?? '' }}</b></span>
                            </li>
                            <li>
                                <span class="fleft">{{ trans('itinerary.duration') }}</span>
                                <span class="fright"><b>{{ $data['tour_info']->tour_duration }}</b> <small>
                                        {{ trans('itinerary.day') }}</small></span>
                            </li>
                            <li>
                                <span class="fleft">{{ trans('itinerary.destinations') }}</span>
                                <span class="fright"><b> {{ $data['numOfAttr'] }} </b> <small>
                                        {{ trans('itinerary.destination') }}</small></span>
                            </li>
                            <li>
                                <span class="fleft">{{ trans('itinerary.comments') }}</span>
                                <span class="fright"><b class="total_reviews">0</b> <small> <ion-icon
                                            name="chatbubbles"></ion-icon></small></span>
                            </li>
                            <li>
                                <span class="fleft">{{ trans('itinerary.rate') }}</span>
                                <span class="fright"><b class="total_rating">0.0</b> <small> <ion-icon
                                            name="podium"></ion-icon></small></span>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-16 list-social-tour tour_comments_container ">
                    <div class="fright text-right btn-group ">
                        <button class="btn btn-default ">
                            <ion-icon name="archive"></ion-icon>
                            <span> <span class="hidden-xs">{{ trans('itinerary.download') }}</span></span>
                            <b class="number_like">0</b>
                        </button>
                        <button class="btn btn-default" style="margin-left: 10px">
                            <ion-icon name="thumbs-up"></ion-icon>
                            <span>{{ trans('itinerary.like') }}</span>
                            <b class="number_like">0</b>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @for ($i = 0; $i < $data['tour_info']->tour_duration; $i++)
                @include('tour-date.tour-date', [
                    'i' => $i + 1,
                    'image' => $data['attr_image'],
                    'tour_detail' => $data['tour_detail'],
                    'numberAttrEachDay' => $data['numberAttrEachDay'],
                ])
            @endfor
        </div>
        <div class="col-md-5">
            <div id="tour-qc" class="tour-qc-box">
                <h4 class="qc-box-title">{{ trans('itinerary.why') }}</h4>
                <ul class="lst-qc-box">
                    <li>
                        <ion-icon name="pin" class="qc-box-icon"></ion-icon>
                        <h5 class="qc-box-ld">{{ trans('itinerary.over') }}</h5>
                        <p>{{ trans('itinerary.uniquetravel') }}</p>
                    </li>
                    <li>
                        <ion-icon name="trending-down"></ion-icon>
                        <h5 class="qc-box-ld">{{ trans('itinerary.lowcost') }}</h5>
                        <p>{{ trans('itinerary.bestdeal') }}</p>
                    </li>
                    <li>
                        <ion-icon name="chatboxes"></ion-icon>
                        <h5 class="qc-box-ld">{{ trans('itinerary.excellent') }}</h5>
                        <p>{{ trans('itinerary.prostaff') }}</p>
                    </li>
                </ul>
            </div>
            {{-- top: 380px; --}}
            <div id="tour-support" class="tour-qc-box" style="">
                <h4 class="qc-box-title">{{ trans('itinerary.needhelp') }}</h4>
                <p class="box-2-p">{{ trans('itinerary.needsp') }}</p>
                <div class="contact-details">
                    <p class="contact-phone">
                        <ion-icon name="call"></ion-icon><b>02293886068</b>
                    </p>
                </div>
            </div>
            {{-- top: 520px; margin-top: 20px; --}}
            <div id="sidemenu-tour" class="tour-qc-box navbar" style=" ">
                <ul class="lst-day-tour nav navbar-nav">
                    <li class="">
                        <a href="#tour_detail_info">
                            <span class="fleft">
                                <ion-icon name="map"></ion-icon>
                                {{ trans('itinerary.itidetail') }}
                            </span>
                        </a>
                    </li>
                    @for ($i = 0; $i < $data['tour_info']->tour_duration; $i++)
                        <li>
                            <a href="#section-{{ $i + 1 }}">
                                <span class="fleft"><ion-icon name="calendar"></ion-icon> {{ trans('itinerary.Day') }}
                                    {{ $i + 1 }}</span>
                                <span class="fright"> {{ $data['numberAttrEachDay'][$i] }}
                                    {{ trans('itinerary.destination') }}</span>
                            </a>
                        </li>
                    @endfor
                </ul>
                <a class="btn btn-primary btn-lg btn-block custom-tour" id="custom-tour"
                    href="/update-tour?tour_id={{ $data['tour_info']->tour_id ?? 1 }}"><ion-icon name="options"></ion-icon>
                    {{ trans('itinerary.custom') }}</a>
            </div>
        </div>
    </div>
    @include('map.mapbox', [
        'attractions' => $data['tour_detail'],
        'duration' => $data['tour_info']->tour_duration,
    ])
    <script>
        mapboxgl.accessToken =
            'pk.eyJ1Ijoia2hhbmhuZ28yMzAzIiwiYSI6ImNsbzV4MHI3NzBkZXkya254NGx4MWVxaDAifQ.2HK_In7H9oW0Nn9dwb9vzw';
    </script>
@endsection
