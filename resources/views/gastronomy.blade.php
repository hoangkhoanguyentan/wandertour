@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/blog.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-detail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
    <script type="text/javascript" src="{{ asset('js/app/itinerary-detail.js') }}"></script>
@endsection
@php
    use Illuminate\Support\Str;
@endphp
@section('body')
    <div class="breadcrumb">
        <div class="container">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ trans('home.Home') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info"
                            href="/gastronomies">{{ trans('home.Gastronomies') }}</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        @if (!empty($featuredPost))
            <div class="col-md-12 no-padding-left no-padding-right" id="tour_detail_info">
                <div id="tour-detail-info-top" style="height: auto; padding: 0px 10px;">
                    @if (!empty($featuredPost))
                        <article class="article-featured">
                            <h2 class="article-title">
                                @if (Session::get('website_language', config('app.locale')) == 'vi')
                                    {{ $featuredPost['title'] }}
                                @else
                                    {{ $featuredPost['en_title'] }}
                                @endif
                            </h2>
                            <img src="{{ asset('image/' . $featuredPost['image_link']) }}"
                                class="article-image feature-image">
                            <p class="article-info"> {{ substr($featuredPost['updated_at'], 0, 10) }}</p>
                            <p class="article-body">
                                <?php
                                $dom = new DOMDocument();
                                $character_limit = 400;
                                libxml_use_internal_errors(true);
                                
                                if (Session::get('website_language', config('app.locale')) == 'vi') {
                                    // Đặt encoding cho DOMDocument
                                    $dom->loadHTML('<?xml encoding="UTF-8">' . $featuredPost['content']);
                                } else {
                                    // Đặt encoding cho DOMDocument
                                    $dom->loadHTML('<?xml encoding="UTF-8">' . $featuredPost['en_content']);
                                }
                                
                                // Đặt encoding cho DOMDocument
                                $dom->loadHTML('<?xml encoding="UTF-8">' . $featuredPost['content']);
                                
                                $description = '';
                                foreach ($dom->getElementsByTagName('*') as $element) {
                                    // Check if the element is an image
                                    if ($element->nodeName === 'img') {
                                        // Skip images
                                        continue;
                                    }
                                
                                    // Extract and append text content to the description
                                    $description .= $element->nodeValue;
                                }
                                
                                $description = strip_tags($description); // Strip any remaining HTML tags
                                if (mb_strlen($description, 'UTF-8') > $character_limit) {
                                    $description = mb_substr($description, 0, $character_limit, 'UTF-8') . '...';
                                }
                                echo "<p>$description</p>"; // This displays the processed description
                                ?>
                            </p>
                            <a href="{{ url('destination/' . $featuredPost['slug']) }}"
                                class="article-read-more">{{ trans('blog.Readmore') }}</a>
                        </article>
                    @else
                        <h2 class="article-title">{{ trans('home.nopost') }}</h2>
                    @endif
                    @if (!empty($remainingPosts))
                        @foreach ($remainingPosts as $post)
                            <article class="article-recent">
                                <div class="article-recent-main">
                                    <h2 class="article-title">
                                        @if (Session::get('website_language', config('app.locale')) == 'vi')
                                            {{ $post['title'] }}
                                        @else
                                            {{ $post['en_title'] }}
                                        @endif
                                    </h2>
                                    <p class="article-body">
                                        <?php
                                        $dom = new DOMDocument();
                                        $character_limit = 400;
                                        
                                        libxml_use_internal_errors(true);
                                        
                                        if (Session::get('website_language', config('app.locale')) == 'vi') {
                                            // Đặt encoding cho DOMDocument
                                            $dom->loadHTML('<?xml encoding="UTF-8">' . $post['content']);
                                        } else {
                                            // Đặt encoding cho DOMDocument
                                            $dom->loadHTML('<?xml encoding="UTF-8">' . $post['en_content']);
                                        }
                                        
                                        $description = '';
                                        foreach ($dom->getElementsByTagName('*') as $element) {
                                            // Check if the element is an image
                                            if ($element->nodeName === 'img') {
                                                // Skip images
                                                continue;
                                            }
                                        
                                            // Extract and append text content to the description
                                            $description .= $element->nodeValue;
                                        }
                                        
                                        $description = strip_tags($description); // Strip any remaining HTML tags
                                        if (mb_strlen($description, 'UTF-8') > $character_limit) {
                                            $description = mb_substr($description, 0, $character_limit, 'UTF-8') . '...';
                                        }
                                        echo "<p>$description</p>"; // This displays the processed description
                                        ?>
                                    </p>
                                    <a href="{{ url('destination/' . $post['slug']) }}"
                                        class="article-read-more">{{ trans('blog.Readmore') }}</a>
                                </div>
                                <div class="article-recent-secondary">
                                    <img src="{{ asset('image/' . $post['image_link']) }}" class="article-image">
                                    <p class="article-info">{{ substr($post['updated_at'], 0, 10) }}</p>
                                </div>
                            </article>
                        @endforeach
                    @endif
                </div>
            </div>
        @else
            <div class="col-md-12 no-padding-left no-padding-right" id="tour_detail_info">
                <div id="tour-detail-info-top" style="height: auto; padding: 0px 10px;">
                    <article class="article-featured">
                        <h2 class="article-title">{{ trans('home.nopost') }}</h2>
                    </article>
                </div>
            </div>
        @endif
        {{-- <div class="col-md-5">
            <div id="tour-qc" class="sidebar tour-qc-box">
                <div class="sidebar-widget">
                    <h2 class="widget-title">Bài viết mới nhất</h2>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Keeping cooking simple</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/food.jpg"
                            alt="two dumplings on a wood plate with chopsticks" class="widget-image">
                    </div>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Simplicity and work</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/work.jpg"
                            alt="a chair white chair at a white desk on a white wall" class="widget-image">
                    </div>
                    <div class="widget-recent-post">
                        <h3 class="widget-recent-post-title">Simple decorations</h3>
                        <img src="https://raw.githubusercontent.com/kevin-powell/reponsive-web-design-bootcamp/master/Module%202-%20A%20simple%20life/img/deco.jpg"
                            alt="a green plant in a clear, round vase with water in it" class="widget-image">
                    </div>
                </div>

            </div>
        </div> --}}
    </div>

    {{-- <div class="container container-flex container-blog">
        <main role="main">

        </main>

        <aside class="sidebar">

        </aside>
    </div> --}}
@endsection
