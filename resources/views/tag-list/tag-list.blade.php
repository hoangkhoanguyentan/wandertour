<fieldset class="filter-group">
    @forelse($tag as $item)
        <div class="checkbox">
            @if (Session::get('website_language', config('app.locale')) == 'vi')
                <input name="filter-interests[]" type="checkbox" id="{{ $item->tag_id }}"
                    {{ !empty($arr_checked) && in_array($item->name, $arr_checked) ? 'checked' : '' }}
                    name-interested="{{ $item->name }}" value="{{ $item->tag_id }}" />
                <label for="{{ $item->tag_id }}">{{ $item->name }}</label>
            @else
                <input name="filter-interests[]" type="checkbox" id="{{ $item->tag_id }}"
                    {{ !empty($arr_checked) && in_array($item->en_name, $arr_checked) ? 'checked' : '' }}
                    name-interested="{{ $item->en_name }}" value="{{ $item->tag_id }}" />
                <label for="{{ $item->tag_id }}">{{ $item->en_name }}</label>
            @endif
        </div>
    @empty
    @endforelse

</fieldset>
