@extends('master')
@section('assets')
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('css/app/user_detail.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/app/function_bar.css')}}">
	<!-- <script type="text/javascript" src="{{asset('js/library/user_detail.js')}}"></script> -->
@endsection
@section('content')
	<div class="container-fluid">
		<div class="function-bar">
			<div class="title">
				<h1>User Detail</h1>
			</div>
			<div class="heading-btn-group">
				<a href="#" class="btn-save">
					<i class="glyphicon glyphicon-ok-sign"></i>
					<span>Save</span>
				</a>
				<a href="#" class="btn-delete">
					<i class="glyphicon glyphicon-remove-sign"></i>
					<span>Delete</span>
				</a>
				<a href="#" class="btn-add-new">
					<i class="glyphicon glyphicon-plus-sign"></i>
					<span>Add new</span>
				</a>
				<a href="#" class="btn-back">
					<i class="glyphicon glyphicon-share-alt"></i>
					<span>Back</span>
				</a>
			</div>
		</div>
	</div>
@endsection