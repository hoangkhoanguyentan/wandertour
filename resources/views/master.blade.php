<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>ChowChow</title>
    <link rel="icon" href="{{ asset('image/danang.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/bootstrap-example.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/prettify.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/master.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/library/jquery-confirm.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/library/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/prettify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/jquery.waypoints.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/library/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app/nav-bar.js') }}"></script>
    {{-- <script src="https://unpkg.com/ionicons@4.4.8/dist/ionicons.js"></script> --}}
    {{--	<script src="{{asset('js/library/test.js')}}"></script> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <x-head.tinymce-config />
    @yield('assets')
</head>

<body>
    @yield('content')

    <footer class="footer-section">
        <div class="container">
            <div class="footer-content pt-5 pb-5">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 mb-50">
                        <div class="footer-widget footer-widget-logo">
                            <div class="footer-logo">
                                <a href="/"><img src="{{ asset('image/danang.png') }}" class="img-fluid"
                                        alt="Danang travel"></a>
                            </div>
                            <div class="footer-text">
                                <p>{{ trans('home.Danang introduce') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 mb-30">
                        <div class="footer-widget footer-widget-link">
                            <div class="footer-widget-heading">
                                <h3>{{ trans('home.Links') }}</h3>
                            </div>
                            <ul>
                                <li><a href="/">{{ trans('home.Home') }}</a></li>
                                <li><a href="/destinations">{{ trans('home.Destination') }}</a></li>
                                <li><a href="/gastronomies">{{ trans('home.Gastronomies') }}</a></li>
                                <li><a href="/personal-itinerary">{{ trans('home.Itinerary') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-8 mb-50">
                        <div class="footer-widget">
                            <div class="footer-widget-heading">
                                <h3>{{ trans('home.Language') }}</h3>
                            </div>
                            <div>
                                <a class="change-languge" href="{!! route('change-language', ['en']) !!}">English</a>
                                <a class="change-languge" href="{!! route('change-language', ['vi']) !!}">Vietnam</a>
                            </div>

                            <div class="footer-widget-heading">
                                <h3>{{ trans('home.Signup') }}</h3>
                            </div>
                            <div class="subscribe-form">
                                <form action="#">
                                    <input type="text" placeholder="{{ trans('home.EmailAddress') }}">
                                    <button><i class="fab fa-telegram-plane"></i></button>
                                </form>
                            </div>
                            <div class="footer-text mb-25">
                                <p>{{ trans('home.Subscribing') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 text-center text-lg-left">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2023, All Right Reserved <a href="#">HKhoa</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
