@extends('master')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('nav-bar.css') }}">
@endsection

@section('content')
    <header>
        <nav class="sticky" style="position: relative">
            <div class="row">
                <img src="{{ asset('image/danang.png') }}" alt="Omnifood logo" class="logo">
                <a href="/"><img src="{{ asset('image/danang.png') }}" alt="Omnifood logo" class="logo-black"></a>
                <ul class="main-nav js--main-nav">
                    @if (Request::is('home'))
                        <li><a href="#destinations">{{ trans('home.Destination') }}</a></li>
                        <li><a href="#gastronomy">{{ trans('home.Gastronomies') }}</a></li>
                        <li><a href="#itinerary">{{ trans('home.Itinerary') }}</a></li>
                    @else
                        <li><a href="/destinations">{{ trans('home.Destination') }}</a></li>
                        <li><a href="/gastronomies">{{ trans('home.Gastronomies') }}</a></li>
                        <li><a href="/personal-itinerary">{{ trans('home.Itinerary') }}</a></li>
                    @endif
                    <?php
                    echo '<li>';
                    if (session()->has('userInfo')) {
                        echo '<a href="/personal-itinerary">' . trans('home.Account') . '</a>';
                    } else {
                        echo '<a href="/login">' . trans('home.Login') . '</a>';
                    }
                    echo '</li>';
                    if (session()->has('userInfo')) {
                        echo '<li>';
                        echo '<a href="/logout">' . trans('home.Logout') . '</a>';
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
        </nav>
    </header>
    @yield('body')
@endsection
