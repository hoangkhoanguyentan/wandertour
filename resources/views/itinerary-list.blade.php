@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-item.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/custom-checkbox.css') }}">
    <script type="text/javascript" src="{{ asset('js/app/itinerary-list.js') }}"></script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{ trans('itinerary.home') }}</a></li>
                    <li class="breadcrumb-item active"><a class="text-info"
                            href="/personal-itinerary">{{ trans('itinerary.itinerary') }}</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="filters">
            <div class="col-md-4 col-sm-16 no-padding">
                <div class="sidebar-scroll">
                    <div class="sidebar">
                        <div class="item-filter">
                            <div class="service">
                                <div class="filter-title main-title-custom">
                                    {{ trans('itinerary.touratt') }}
                                </div>
                            </div>
                            <div class="service-1 service-data">
                                <fieldset class="filter-group" style="">
                                    <select name="travel_num_day" id="travel_place" class="form-control">
                                        <option selected="selected" value="1">{{ trans('itinerary.dncity') }},
                                            {{ trans('itinerary.vn') }}</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="item-filter">
                            <div class="service" data-service="2">
                                <div class="filter-title main-title-custom">
                                    {{ trans('itinerary.days') }}
                                </div>
                                <span class="ion-minus-circled filter-icon"></span>
                            </div>
                            <div class="service-2 service-data">
                                <fieldset class="filter-group">
                                    <select class="form-control" name="travel_num_day" id="travel_num_day">
                                        <option value="0" selected></option>
                                        @for ($i = 1; $i <= 5; $i++)
                                            <option value="{{ $i }}" name="{{ $i }}"
                                                {{ $data['duration'] == $i ? 'selected' : '' }}>{{ $i }}
                                                {{ trans('itinerary.day') }}</option>
                                        @endfor

                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="item-filter">
                            <div class="service" data-service="4">
                                <div class="filter-title main-title-custom">
                                    {{ trans('itinerary.interests') }}
                                </div>
                                <span class="ion-minus-circled filter-icon"></span>
                            </div>
                            <div class="service-4 service-data">
                                @include('tag-list.tag-list', [
                                    'tag' => $data['tag'],
                                    'arr_checked' => $data['interested'] ?? [],
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-16 row no-padding-left">
                @include('itinerary-item.section-result', [
                    'data' => $data,
                ])
            </div>
        </div>
    </div>
    <script>
        var interested_checked = {!! json_encode($data['interested']) !!};
    </script>
@endsection
