<div class="col-sm-6 col-md-5 col-lg-3 no-padding-right">
    <div class="thumbnail-grid thumbnail ">
        <div class="thumbnail-img thumbscrubber">
            <a href="#">
                <img src="{{ asset('image/' . $item->link) }}">
            </a>
        </div>
        <div class="thumbnail-body">
            <div class="caption">
                <a href="#" class="prop-title">
                    {{ $item->tour_name ?? '' }}
                </a>
                {{-- <div class="rating col-xs-5">
                    <span class="sprite star-ora"></span>
                    <span class="sprite star-ora"></span>
                    <span class="sprite star-ora"></span>
                    <span class="sprite star-ora"></span>
                    <span class="sprite star"></span>
                </div> --}}
                {{-- <div class="tl-right col-nopadding col-xs-7 pull-right">
                    <span class="sprite eye"></span>353
                    <span class="sprite download"></span>2
                </div> --}}
            </div>
            <div class="thumbnail-content ">
                <ul class="list-unstyled feature-list">
                    <li>
                        <span>{{ trans('home.Amount') }}</span>
                        <label class="pull-right money">
                            {{ $item->tour_budget = 0 ? trans('itinerary.free') : $item->tour_budget }}
                            <small>VNĐ</small>
                        </label>
                    </li>
                    <li>
                        <span>{{ trans('home.Duration') }}</span>
                        <label class="pull-right">
                            {{ $item->tour_duration ?? '' }}
                            <small>{{ trans('home.days') }}</small>
                        </label>
                    </li>
                    <div class="clearfix"></div>
                </ul>

            </div>
            <div class="link-action">
                <a href="#" title="" class="col-xs-6 text-center tipB"
                    tour-id="{{ $item->tour_id }}">{{ trans('home.ViewItinerary') }}</a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
