<div id="section-result">
    <?php
        if (!array_key_exists('search_result', $data)) {
            $data['search_result'] = array();
        }
    ?>

    @forelse($data['search_result'] as $item)
        @include('itinerary-item.itinerary-item', [
            'item' => $item,
        ])
    @empty
        @include('errors.no-result')
    @endforelse
</div>
