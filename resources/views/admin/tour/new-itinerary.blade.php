@extends('header-sticky.header-sticky')
@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/itinerary-custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/nav-bar.css') }}">
    <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="{{ asset('css/library/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/library/perfect-scrollbar.css') }}" rel="stylesheet" />

    <script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
    <script src="{{ asset('js/library/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app/itinerary-new.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/mustacle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/perfect-scrollbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/library/sortable.js') }}"></script>
    <script>
        var duration = {!! json_encode($data['tour_info']->tour_duration) !!};
        var mode = {!! json_encode($data['mode']) !!};
        var lang = {!! json_encode(Session::get('website_language')) !!};
    </script>
@endsection

@section('body')
    <div class="breadcrumb">
        <div class="container">
            <div class="col-md-16 pd-l-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">
                            {{ trans('itinerary.home') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="text-info" href="#">
                            {{ trans('itinerary.itinerary') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a class="text-info" href="#">
                            {{ trans('itinerary.custombreadcrumb') }}
                        </a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 no-padding tour-info">
                <div class="form-group col-md-5">
                    <label for="tour-name">{{ trans('itinerary.itiname') }}</label>
                    <input type="text" class="form-control" id="tour-name"
                        value="{{ $data['tour_info']->tour_name ?? '' }}">
                </div>
                <div class="form-group col-md-2">
                    <label for="start-date">{{ trans('itinerary.start') }}</label>
                    <div class="input-group date start-date-class" id="">
                        <input type="text" class="form-control" id="start-date"
                            value="{{ $data['tour_info']->start_date ?? '' }}" onblur="startDateChange(this)">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar">
                            </span>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <label for="end-date">{{ trans('itinerary.end') }}</label>
                    <div class="input-group date end-date-class" id="">
                        <input type="text" class="form-control" id="end-date"
                            value="{{ $data['tour_info']->end_date ?? '' }}" onblur="endDateChange(this)">

                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar">
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="transport">{{ trans('itinerary.transport') }}</label>
                    <div class="input-group">
                        <select class="form-control" id="transport">
                            @forelse($data['transport'] as $k => $item)
                                <option value="{{ $k }}"
                                    {{ isset($data['tour_transport']->transport) && $k == $data['tour_transport']->transport ? 'selected' : '' }}>
                                    {{ $item }}
                                </option>
                            @empty
                                <option value=""></option>
                            @endforelse
                        </select>
                        <span class="input-group-btn"><!-- Append button addon using class input-group-lg -->
                            <button class="btn btn-success" type="button" id="btn-transport">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="interested">{{ trans('itinerary.interests') }} </label>
                    <select id="interested" name="interested[]" multiple="multiple" class="form-control">
                        @forelse($data['interested'] as $item)
                            <option
                                @if (!empty($data['tour_tag'])) 
                                    @foreach ($data['tour_tag'] as $obj)
                                        {{ $obj->tag_id == $item->tag_id ? 'selected' : '' }}
                                    @endforeach 
                                @endif
                                value="{{ $item->tag_id }}">
                                @if (Session::get('website_language') == 'en')
                                    {{ $item->en_name ?? '' }}
                                @else
                                    {{ $item->name ?? '' }}
                                @endif
                            </option>
                        @empty
                            <p>There are no users yet!</p>
                        @endforelse
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="adult-number">{{ trans('itinerary.people') }}</label>
                    <input type="number" min="1" max="10" class="form-control" id="adult-number"
                        value="{{ $data['tour_info']->people ?? '' }}">
                </div>
                <div class="col-md-3">
                    <label for="hotel">{{ trans('itinerary.hotelmotel') }} </label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="hotel"
                            value="{{ $data['tour_accommodation']->hotel_name ?? '' }}">
                        <span class="input-group-btn"><!-- Append button addon using class input-group-lg -->
                            <button class="btn btn-success" type="button" id="btn-accommodation">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container mt-10">
        <div class="row">
            <div class="col-md-8 bg-white" id="tour-detail">
                @for ($i = 0; $i < $data['tour_info']->tour_duration; $i++)
                    @include('admin.tour.tour-custom.itinerary-edit', [
                        'i' => $i + 1,
                        'tour_detail' => $data['tour_detail'],
                        'numberAttrEachDay' => $data['numberAttrEachDay'],
                    ])
                @endfor
            </div>
            <div class="col-md-4 bg-white mg-l-10 no-padding-left no-padding-right list-attraction-bar">
                <div id="search-place-tour">
                    <ul class="nav nav-pills mg-b-6">
                        <li class="active">
                            <a data-toggle="pill" href="#places" onclick="findAttractionByTag('places')">
                                {{ trans('itinerary.descall') }}
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="pill" href="#restaurants" onclick="findAttractionByTag('restaurants')">
                                {{ trans('itinerary.gascall') }}
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="pill" href="#shop" onclick="findAttractionByTag('shop')">
                                {{ trans('itinerary.shopcall') }}
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="pill" href="#hotels" onclick="findAttractionByTag('hotels')">
                                {{ trans('itinerary.acccall') }}
                            </a>
                        </li>
                    </ul>
                    <div class="input-group filter-search">
                        <input type="text" class="form-control input-sm input-findname" name="search-attraction">
                        <div class="input-group-btn">
                            <button class="btn-sm btn-search"><ion-icon name="search"></ion-icon></button>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="places" class="">
                            <div class="lst-item-form" id="tab-panel-1" style="overflow: hidden">
                                @include('admin.tour.tour-custom.tab-panel', ['data' => $data['attractions']])
                                <div class="clearfix"></div>
                            </div>
                            <a class="btn btn-primary btn-lg btn-block custom-tour" id="save-tour"><ion-icon
                                    name="options"></ion-icon> {{ trans('itinerary.saveiti') }}</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @if (empty($data['tour_transport']))
        @include('admin.tour.popup.transport-popup')
    @else
        @include('admin.tour.popup.transport-popup', ['data' => $data['tour_transport']])
    @endif
    @if (empty($data['tour_accommodation']))
        @include('admin.tour.popup.accommodation-popup')
    @else
        @include('admin.tour.popup.accommodation-popup', ['data' => $data['tour_accommodation']])
    @endif
    <script id="item_tour_detail" type="x-tmpl-mustache">
        <li class="li-parent" data-time-open="@{{TimeOpen1}}" data-time-close="@{{TimeClose}}" data-geo="@{{Geo}}" data-id="@{{PlaceInfo}}"  data-visit-time="@{{VisitTime}}">
            <div class="place-tour-img">
                <img id="attr_img_link" src="@{{Image}}" onerror="this.src='/Images/NoImage/Transparency/NoImage400x266.png'">
            </div>
            <div class="place-tour-info">
                <p class="name mg-b-0" id="attr_img_name">@{{PlaceName}}</p>
                <div class="place-tour-info-time">
                    <div class="time_o">
                        <div><i class="ion-clock"></i> @{{tranOpenTime}}: <span class="time" id="opening_time_start">@{{TimeOpen}}</span></div>
                        <div><i class="ion-clock"></i> @{{tranCloseTime}}: <span class="time" id="opening_time_end">@{{TimeClose}}</span></div>
                    </div>
                    <span class="time change-time">@{{Time}}</span>
                    <span class="time-error"></span>
                </div>
                <div class="place-tour-action remove-place"><ion-icon name="close"></ion-icon></div>
            </div>
            <div class="calculater-route-info"></div>
        </li>
    </script>
@endsection
