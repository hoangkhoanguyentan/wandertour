<div class="main-view">
    <script>
        var duration = {!! json_encode($data['tour_info']['duration']) !!};
    </script>
    @for ($i = 0; $i < $data['tour_info']['duration']; $i++)
        @include('admin.tour.tour-custom.new-itinerary-edit', [
            'i' => $i + 1,
            'tour_detail' => $data['tour_detail'],
        ])
    @endfor
</div>

<script>
    var directionsService;
    var regex = /:/gi;

    var tranOpenTime = "Mở cửa";
    var enTranOpenTime = "Open";

    var tranCloseTime = "Đóng cửa";
    var enTranCloseTime = "Close";


    $(document).ready(function() {
        try {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            initialize();
            initEvent();
            $("#tour-name").focus();

            var tourName = document.getElementById("tour-name");
        } catch (e) {
            window.alert("document " + e);
        }
    });
    // initialize object
    function initialize() {
        try {
            $("#interested").select2();
            initDateTimePicker();
            var ps = new PerfectScrollbar(".lst-item-form");
            // var ps = new PerfectScrollbar('');
            var html = Mustache.render($("#item_tour_detail").html(), {
                Geo: 1,
                DataNation: 1,
                Day: 1,
                PlaceInfo: 1,
                Image: 1,
                Time: 11,
                PlaceName: 11,
                TimeOpen1: 22,
                TimeOpen: 33,
                TimeClose: 44,
                VisitTime: 55,
            });
            $(".list-attraction-bar").waypoint(
                function(direction) {
                    if (direction === "down") {
                        $(".list-attraction-bar").addClass(
                            "list-attraction-bar-sticky"
                        );
                    } else {
                        $(".list-attraction-bar").removeClass(
                            "list-attraction-bar-sticky"
                        );
                    }
                }, {
                    offset: "50px",
                }
            );
            $(document).on("click", "#save-tour", function() {
                save();
            });
        } catch (e) {
            window.alert("initialize " + e);
        }
    }
    // initialize object event
    function updateTimeEvent() {
        $(".time").each(function() {
            var time = $(this).text().replace(/\s/g, ""); // Loại bỏ khoảng trắng
            // Kiểm tra xem chuỗi đã chứa dấu ":" hay chưa
            if (time.indexOf(":") === -1) {
                // Nếu chưa có dấu ":", thêm dấu ":" vào sau 2 ký tự đầu tiên
                time = [time.slice(0, 2), ":", time.slice(2)].join("");
            } else {
                // Nếu đã có dấu ":", loại bỏ tất cả các dấu ":" ngoại trừ dấu ":" đầu tiên
                time = time.replace(/:/g, (match, offset, string) => {
                    return offset === string.indexOf(":") ? ":" : "";
                });
            }
            $(this).text(time);
        });
    }

    function initEvent() {
        try {
            updateTimeEvent();
            //init drop list item place search right
            Sortable.create(document.getElementById("tab-panel-1"), {
                sort: false,
                group: {
                    name: "place_item",
                    pull: "clone",
                    put: false,
                },
                animation: 150,
                scroll: true,
            });

            $(document).on("change", "#hotel", function() {
                $("#hotel-name").val($(this).val());
            });
            $(document).on("change", "#hotel-name", function() {
                $("#hotel").val($(this).val());
            });

            setTimeout(initDatePlaceSortDrop(), 200);

        } catch (e) {
            window.alert("initEvent " + e);
        }
    }
    // init drop list date
    function initDatePlaceSortDrop() {
        for (var i = 1; i <= duration; i++) {
            Sortable.create(document.getElementById("lst_place_date_" + i), {
                sort: true,
                group: {
                    name: "place_item",
                    pull: false,
                    put: true,
                },
                scroll: true,
                animation: 250,
                onAdd: function(evt) {
                    var newIDItem =
                        evt.item.attributes.getNamedItem("data-id").value;
                    var imgSrc = evt.item.childNodes[1].children[0].src;
                    var placeName = evt.item.children[1].children[0].textContent;
                    var areaDay = evt.target.attributes
                        .getNamedItem("id")
                        .value.split("_")[3];
                    // var data_tour = evt.item.attributes.getNamedItem("data-tour").value;
                    var data_geo =
                        evt.item.attributes.getNamedItem("data-geo").value;
                    var timeopen =
                        evt.item.attributes.getNamedItem("data-time-open").value;
                    var timeclose =
                        evt.item.attributes.getNamedItem("data-time-close").value;
                    var visittime =
                        evt.item.attributes.getNamedItem("data-visit-time").value;
                    var arr = {
                        Geo: data_geo,
                        Day: areaDay,
                        PlaceInfo: newIDItem,
                        Image: imgSrc,
                        PlaceName: placeName,
                        TimeOpen1: timeopen1,
                        TimeOpen: timeopen1,
                        TimeClose: timeclose,
                        tranOpenTime: lang === "vi" ?
                            tranOpenTime : enTranOpenTime,
                        tranCloseTime: lang === "vi" ?
                            tranCloseTime : enTranCloseTime,
                        VisitTime: visittime,
                    };
                    var lstDayOfTourOnMapOnAdd = [];
                    var arrayCheck = [];
                    $("#lst_place_date_" + areaDay)
                        .find("li.li-parent")
                        .each(function() {
                            var itemAttr = $(this).attr("data-id");
                            if (itemAttr === newIDItem) {
                                arrayCheck.push(newIDItem);
                            }
                        });
                    if (
                        evt.from.id === "tab-panel-1" ||
                        evt.from.id === "list_place_res" ||
                        evt.from.id === "list_place_hotel" ||
                        evt.from.id === "list_place_shop"
                    ) {
                        if (arrayCheck.length > 0) {
                            var itemAdd = $(evt.item);
                            itemAdd.remove();
                            // bootbox.alert({
                            //     title: "Thông báo",
                            //     message: "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!",
                            // });
                            $.alert({
                                title: lang == "vi" ? "Thông báo" : "Notification",
                                content: lang == "vi" ?
                                    "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!" :
                                    "Cannot choose two same places in one day of the itinerary!",
                            });
                        } else {
                            var timeopen1;
                            if (timeopen === "00:00") {
                                timeopen1 = "07:30";
                            } else {
                                timeopen1 = timeopen;
                            }
                            var html = Mustache.render(
                                $("#item_tour_detail").html(), {
                                    Geo: data_geo,
                                    // DataNation: data_tour,
                                    Day: areaDay,
                                    PlaceInfo: newIDItem,
                                    Image: imgSrc,
                                    Time: timeopen1,
                                    PlaceName: placeName,
                                    TimeOpen1: timeopen1,
                                    TimeOpen: timeopen1,
                                    TimeClose: timeclose,
                                    tranOpenTime: lang === "vi" ? tranOpenTime : enTranOpenTime,
                                    tranCloseTime: lang === "vi" ? tranCloseTime : enTranCloseTime,
                                    VisitTime: visittime,
                                }
                            );
                            evt.item.outerHTML = html;
                            $("#lst_place_date_" + areaDay)
                                .find("li.li-parent")
                                .each(function() {
                                    lstDayOfTourOnMapOnAdd.push({
                                        location: $(this).attr("data-geo"),
                                    });
                                });
                        }
                    }
                    updateTimeEvent();
                },
                onSort: function(evt) {
                    var lstDayOfTourOnMapOnAdd = [];
                    var areaDay = evt.to.attributes[1].value.split("_")[3];
                    $("#lst_place_date_" + areaDay)
                        .find("li.li-parent")
                        .each(function() {
                            lstDayOfTourOnMapOnAdd.push({
                                location: $(this).attr("data-geo"),
                            });
                        });
                    $("#lst_place_date_" + areaDay + " li.li-parent:last-child")
                        .children(".calculater-route-info")
                        .html("");
                },
            });
        }
    }
</script>
