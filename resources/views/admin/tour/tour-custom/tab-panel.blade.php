@forelse($data as $item)
    <div class="place-tour-edit" data-time-open="{{ $item->opening_time_start ?? '' }}"
        data-time-close="{{ $item->opening_time_end ?? '' }}" data-geo="{{ $item->lat }},{{ $item->long }}"
        data-id="{{ $item->attr_id }}" data-visit-time="{{ $item->attr_duration }}">
        <div class="place-tour-img">
            <img src="{{ asset('image/' . $item->link) }}">
        </div>
        <div class="place-tour-info">
            @if (Session::get('website_language') == 'en')
                <p class="name">{{ $item->en_attr_name ?? '' }}</p>
            @else
                <p class="name">{{ $item->attr_name ?? '' }}</p>
            @endif

            @if (Session::get('website_language') == 'en')
                <p class="address"><ion-icon name="pin"></ion-icon>{{ $item->en_attr_address ?? '' }}</p>
            @else
                <p class="address"><ion-icon name="pin"></ion-icon>{{ $item->attr_address ?? '' }}</p>
            @endif

        </div>
    </div>
@empty
    <div class="place-tour-edit">
        <div class="place-tour-edit" data-time-open="0700" data-time-close="1800" data-geo="20.285123,105.906594"
            data-id="1">
            <div class="place-tour-img">
                <img
                    src="https://media.dalatcity.org//Images/NBH/superadminportal.nbh/DiaDiem\Co Do\636577349446110186_0.jpg">
            </div>
            <div class="place-tour-info">
                <p class="name">Cố đô Hoa Lư</p>
                <p class="address"><ion-icon name="pin"></ion-icon> Trường Yên Hoa Lư Ninh Bình Việt Nam</p>
            </div>
        </div>
    </div>
@endforelse
