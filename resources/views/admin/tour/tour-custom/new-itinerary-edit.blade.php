<div class="tour-item-edit tour-item-edit-header mt-10">
    <div class="tour-item-timeline" id="edit-tour-{{ $i }}" date-no="{{ $i }}">
        <div class="tour-item-timeline-header">
            <ion-icon name="calendar" class="tour-item-timeline-icon"></ion-icon>
            <div class="tour-item-timeline-title">{{ trans('itinerary.Day') }} {{ $i }}</div>
        </div>
        <div class="tour-item-timeline-body">
            <ul class="lst-date-item" id="lst_place_date_{{ $i }}" date-no="{{ $i }}">
                @if (!empty($data['tour_detail']))
                    @foreach ($data['tour_detail'] as $detail)
                        @if ($i == $detail['day'])
                            <li class="li-parent" data-time-open="{{ $detail['opening_time_start'] }}"
                                data-time-close="{{ $detail['opening_time_end'] }}" data-id="{{ $detail['attr_id'] }}">
                                @include('admin.tour.tour-custom.timeline', [
                                    'itinerary' => $detail,
                                ])
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>
