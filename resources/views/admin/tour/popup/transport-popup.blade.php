<div class="modal fade" id="transport-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">{{ trans('itinerary.transportation') }}</h4>
            </div>
            <div class="modal-body no-padding">
                <div class="container">
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.tranfrom') }}</label>
                            <input type="text" class="form-control" value="{{ $data->departure ?? '' }}"
                                id="transport-from">
                        </div>
                        <div class="form-group col-md-2 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.departure') }}</label>
                            <div class="input-group date transport-start-date" id="">
                                <input type="text" class="form-control" id="transport-start-date"
                                    value="{{ $data->departure_date ?? '' }}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="tour-name">{{ trans('itinerary.time') }}</label>
                            <div class="input-group date transport-start-time" id="">
                                <input type="text" class="form-control" id="transport-start-time"
                                    value="{{ $data->departure_time ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.transportation') }}</label>
                            <input type="text" class="form-control" id="transport-to"
                                value="{{ trans('itinerary.dncity') }}" readonly>
                        </div>
                        <div class="form-group col-md-2 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.return') }}</label>
                            <div class="input-group date transport-end-date" id="">
                                <input type="text" class="form-control" id="transport-end-date"
                                    value="{{ $data->arrival_date ?? '' }}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="tour-name">{{ trans('itinerary.time') }}</label>
                            <div class="input-group date transport-end-time" id="">
                                <input type="text" class="form-control" id="transport-end-time"
                                    value="{{ $data->arrival_time ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.flightno') }}</label>
                            <input type="text" class="form-control" id="number-transport"
                                value="{{ $data->number ?? '' }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="tour-name">{{ trans('itinerary.bookingno') }}</label>
                            <input type="text" class="form-control" id="number-booking"
                                value="{{ $data->booking_no ?? '' }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                    data-dismiss="modal">{{ trans('itinerary.cancel') }}</button>
                <button type="button" class="btn btn-success"
                    id="btn-transport-save">{{ trans('itinerary.save') }}</button>
            </div>
        </div>
    </div>
</div>
