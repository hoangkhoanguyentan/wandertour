<div class="modal fade" id="accommodation-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">{{ trans('itinerary.hotelmotel') }}</h4>
            </div>
            <div class="modal-body no-padding">
                <div class="container">
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">
                                {{ trans('itinerary.name') }} {{ trans('itinerary.hotelmotel') }}
                            </label>
                            <input type="text" class="form-control" id="hotel-name"
                                value="{{ $data->hotel_name ?? '' }}">
                        </div>
                        <div class="form-group col-md-2 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.checkin') }}</label>
                            <div class="input-group date check-in-date" id="">
                                <input type="text" class="form-control" id="check-in-date"
                                    value="{{ $data->checkin_date ?? '' }}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="tour-name">{{ trans('itinerary.time') }}</label>
                            <div class="input-group date check-in-time" id="">
                                <input type="text" class="form-control" id="check-in-time"
                                    value="{{ $data->checkin_time ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.address') }}</label>
                            <input type="text" class="form-control" id="hotel-address"
                                value="{{ $data->address ?? '' }}">
                        </div>
                        <div class="form-group col-md-2 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.checkout') }}</label>
                            <div class="input-group date check-out-date" id="">
                                <input type="text" class="form-control" id="check-out-date"
                                    value="{{ $data->checkout_date ?? '' }}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="tour-name">{{ trans('itinerary.time') }}</label>
                            <div class="input-group date check-out-time" id="">
                                <input type="text" class="form-control" id="check-out-time"
                                    value="{{ $data->checkout_time ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3 no-padding-right">
                            <label for="tour-name">{{ trans('itinerary.roomno') }}</label>
                            <input type="text" class="form-control" id="room-number"
                                value="{{ $data->room ?? '' }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="tour-name">{{ trans('itinerary.bookingno') }}</label>
                            <input type="text" class="form-control" id="number-booking-hotel"
                                value="{{ $data->booking_no ?? '' }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                    data-dismiss="modal">{{ trans('itinerary.cancel') }}</button>
                <button type="button" class="btn btn-success"
                    id="btn-accommodation-save">{{ trans('itinerary.save') }}</button>
            </div>
        </div>
    </div>
</div>
