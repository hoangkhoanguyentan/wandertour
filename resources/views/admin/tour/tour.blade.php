@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
@endsection

<div class="manage-title">
    <h3>Quản lý tour</h3>
    <button onclick="addTour()" class="btn btn-primary add-new">Thêm mới</button>
</div>
<table class="table">
    <thead>
        <tr>
            <th class="tour-no">STT</th>
            <th class="tour-name">Tên tour</th>
            <th class="tour-duration">Thời gian</th>
            <th class="tour-budget">Chi phí</th>
            <th class="tour-person">Số người</th>
            <th class="tour-start-date">Ngày bắt đầu</th>
            <th class="tour-end-date">Ngày kết thúc</th>
            <th class="tour-action">Hành động</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['tour'] as $i => $tour)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $tour->tour_name }}</td>
                <td style="text-align: center">{{ $tour->tour_duration . ' ngày' }}</td>
                <td style="text-align: center">{{ $tour->tour_budget ? $tour->tour_budget : '-' }}</td>
                <td style="text-align: center">{{ $tour->tour_person ? $tour->tour_person : '-' }}</td>
                <td>{{ $tour->start_date }}</td>
                <td>{{ $tour->end_date }}</td>
                <td>
                    <a onclick="editTour({{ $tour->tour_id }})" class="btn btn-warning">Edit</a>
                    <a onclick="confirmDeleteTour({{ $tour->tour_id }})" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    function editTour(id) {
        window.location.href = "/update-tour?tour_id=" + id;
    }

    function addTour() {
        window.location.href = "/itinerary-create";
    }
</script>
