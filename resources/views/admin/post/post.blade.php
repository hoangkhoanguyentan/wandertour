@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
@endsection

<div class="manage-title">
    <h3>Quản lý bài viết ẩm thực</h3>
    <button onclick="addPost()" class="btn btn-primary add-new">Thêm mới</button>
</div>
<table class="table">
    <thead>
        <tr>
            <th class="post-no">STT</th>
            <th class="post-name">Tên món ăn</th>
            <th class="post-title">Tiêu đề bài viết</th>
            <th class="post-display">Hiển thị</th>
            <th class="post-action">Hành động</th>
        </tr>
    </thead>
    <tbody>
        @if (empty($data['post']))
            <tr>
                <td colspan="5" style="text-align: center">Không có bài viết nào</td>
            </tr>
        @else
            @foreach ($data['post'] as $i => $post)
                <tr>
                    <td>{{ $i + 1 }}</td>
                    <td>{{ $post->name }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->display }}</td>
                    <td>
                        <a onclick="editPost({{ $post->id }})" class="btn btn-warning">Edit</a>
                        <a onclick="confirmDeletePost({{ $post->id }})" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
