<div class="manage-title">
    <h3>Chỉnh sửa bài viết</h3>
</div>
<div class="update-container">
    <div class="body-content">
        <div class="formm">
            <form enctype="multipart/form-data" method="post" action="{{ route('save-post') }}">
                @csrf
                <div class="form-group">
                    <label>Ảnh</label>
                    <input type="text" class="form-control" id="img_id" name="img_id" style="display: none;"
                        value="{{ $data['img_id'] ?? '' }}">
                    <label class="picture" for="picture__input" tabIndex="0">
                        <span class="picture__image">
                            @if (!empty($data['img_link']))
                                <img id="picture__image__preview" src="{{ asset('image/' . $data['img_link']) }}"
                                    class="picture__img">
                            @else
                                <img id="picture__image__preview" class="picture__img">
                            @endif
                        </span>
                    </label>

                    <input type="file" name="picture__input" id="picture__input"
                        @if (empty($data['img_id'])) required @endif>
                    @error('picture__input')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Tên món ăn</label>
                        <input type="text" class="form-control" name="id" id="id"
                            value="{{ $data['id'] ?? '' }}" style="display: none;">
                        <input type="text" class="form-control" name="mode" id="mode"
                            value="{{ $data['mode'] }}" style="display: none;">
                        <input type="text" class="form-control" name="name" id="name"
                            value="{{ $data['name'] ?? '' }}">
                    </div>

                    <div class="col-md-6">
                        <label>Name of food</label>
                        <input type="text" class="form-control" name="en_name" id="en_name"
                            value="{{ $data['en_name'] ?? '' }}">
                    </div>
                    <div class="col-md-6">
                        <label>Hiển thị trên trang chủ</label>
                        <select class="form-control" name="display" id="display">
                            <option value="1" @if ($data['display'] == 1) selected @endif>Hiển thị</option>
                            <option value="0" @if ($data['display'] == 0) selected @endif>Không hiển thị
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Tiêu đề bài viết</label>
                    <input type="text" class="form-control" name="title" id="title"
                        value="{{ $data['title'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Post title</label>
                    <input type="text" class="form-control" name="en_title" id="en_title"
                        value="{{ $data['en_title'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Nội dung</label>
                    <textarea name="content" id="content">{{ $data['content'] ?? '' }}</textarea>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea name="en_content" id="en_content">{{ $data['en_content'] ?? '' }}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">
                        @if ($data['mode'] == 'I')
                            Thêm mới
                        @else
                            Cập nhật
                        @endif
                    </button>
                    <button class="btn btn-danger" id="cancel">
                        Hủy bỏ
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    tinymce.init({
        selector: 'textarea#content',
        plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Admin',
        height: 300,
        mergetags_list: [{
                value: 'Admin',
                title: 'Admin'
            },
            {
                value: 'Email',
                title: 'Email'
            },
        ],
        ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
            "See docs to implement AI Assistant"))
    });

    tinymce.init({
        selector: 'textarea#en_content',
        plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Admin',
        height: 300,
        mergetags_list: [{
                value: 'Admin',
                title: 'Admin'
            },
            {
                value: 'Email',
                title: 'Email'
            },
        ],
        ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
            "See docs to implement AI Assistant"))
    });

    $(document).ready(function() {
        try {
            // $(document).on('click', '#save-blog', function() {
            //     save();
            // });
            $(document).on('click', '#cancel', function() {
                console.log('Click cancel gas to menu post')
                location.href = '/personal-itinerary?menu=post';
                return false;
            });

            const inputFile = document.querySelector("#picture__input");

            inputFile.addEventListener("change", function(e) {
                const inputTarget = e.target;
                const file = inputTarget.files[0];

                if (file) {
                    const reader = new FileReader();

                    reader.addEventListener("load", function(e) {
                        const readerTarget = e.target;

                        const img = document.getElementById("picture__image__preview");
                        img.src = readerTarget.result;
                        img.classList.add("picture__img");
                    });
                    reader.readAsDataURL(file);
                }
            });
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });
</script>
