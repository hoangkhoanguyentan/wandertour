@section('assets')
    <script type="text/javascript" src="{{ asset('js/app/admin.js') }}"></script>
@endsection

<div class="manage-title">
    <h3>Quản lý địa điểm</h3>
    <div class="button">
        <button onclick="importAttraction()" class="btn btn-success import-button">Nhập file</button>
        <button onclick="addAttraction()" class="btn btn-primary add-new">Thêm mới</button>
    </div>

</div>
<table class="table">
    <thead>
        <tr>
            <th class="des-name">Tên</th>
            <th class="des-addr">Địa chỉ</th>
            {{-- <th>Tọa độ</th> --}}
            <th class="des-time">Thời gian mở - đóng cửa</th>
            <th class="des-blog">Bài viết</th>
            <th>Giá</th>
            <th class="des-action">Hành động</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['attraction'] as $attr)
            <tr>
                <td>{{ $attr->attr_name }}</td>
                <td>{{ $attr->attr_address }}</td>
                {{-- <td>{{ $attr->lat }}, {{ $attr->long }}</td> --}}
                <td>
                    <span class="time">{{ $attr->opening_time_start }}</span> -
                    <span class="time">{{ $attr->opening_time_end }}</span>
                </td>
                <td>
                    @if (!empty($attr['blog']))
                        Có
                    @else
                        Chưa
                    @endif
                </td>
                <td style="text-align: center">{{ $attr->price ? $attr->price . ' VND' : '-' }} </td>
                <td>
                    <a onclick="editBlog({{ $attr->attr_id }})" class="btn btn-primary">
                        @if (!empty($attr['blog']))
                            Sửa
                        @else
                            Viết
                        @endif
                        bài
                    </a>
                    <a onclick="editAttraction({{ $attr->attr_id }})" class="btn btn-warning">Sửa</a>
                    <a onclick="confirmDeleteAttraction({{ $attr->attr_id }})" class="btn btn-danger">Xóa</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function() {
        try {
            $(".time").each(function() {
                var time = $(this).text().replace(/\s/g, ""); // Loại bỏ khoảng trắng
                // Kiểm tra xem chuỗi đã chứa dấu ":" hay chưa
                if (time.indexOf(":") === -1) {
                    // Nếu chưa có dấu ":", thêm dấu ":" vào sau 2 ký tự đầu tiên
                    time = [time.slice(0, 2), ":", time.slice(2)].join("");
                } else {
                    // Nếu đã có dấu ":", loại bỏ tất cả các dấu ":" ngoại trừ dấu ":" đầu tiên
                    time = time.replace(/:/g, (match, offset, string) => {
                        return offset === string.indexOf(":") ? ":" : "";
                    });
                }
                $(this).text(time);
            });
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });
</script>
