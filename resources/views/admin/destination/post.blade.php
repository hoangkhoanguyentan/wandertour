<div class="manage-title">
    <h3>Chỉnh sửa bài viết</h3>
</div>
<div class="update-container">
    <div class="body-content">
        <div class="formm">
            <form id="desination">
                @if (Session::get('website_language') == 'vi')
                    <div class="form-group">
                        <label>Tên địa điểm</label>
                        <input type="text" class="form-control" id="mode" value="{{ $data['mode'] }}"
                            style="display: none;">
                        <input type="text" class="form-control" id="attr-id" value="{{ $data['attr_id'] }}"
                            style="display: none;">
                        <input disabled type="text" class="form-control" name="attr-name" id="attr-name"
                            value="{{ $data['attr_name'] ?? '' }}">
                    </div>
                @else
                    <div class="form-group">
                        <label>Attraction name</label>
                        <input type="text" class="form-control" id="mode" value="{{ $data['mode'] }}"
                            style="display: none;">
                        <input type="text" class="form-control" id="attr-id" value="{{ $data['attr_id'] }}"
                            style="display: none;">
                        <input disabled type="text" class="form-control" name="en_attr-name" id="en_attr-name"
                            value="{{ $data['en_attr_name'] ?? '' }}">
                @endif
        </div>
        <div class="form-group">
            <label>Tiêu đề</label>
            <input type="text" class="form-control" name="title" id="title"
                value="{{ $data['blog']->title ?? '' }}">
        </div>
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" name="en_title" id="en_title"
                value="{{ $data['blog']->en_title ?? '' }}">
        </div>

        <div class="form-group">
            <label>Nội dung</label>
            <textarea id="content">{{ $data['blog']->content ?? '' }}</textarea>
            {{-- <x-forms.tinymce-editor /> --}}
        </div>

        <div class="form-group">
            <label>Content</label>
            <textarea id="en_content">{{ $data['blog']->en_content ?? '' }}</textarea>
            {{-- <x-forms.tinymce-editor /> --}}
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">
                @if ($data['mode'] == 'I')
                    Thêm mới
                @else
                    Cập nhật
                @endif
            </button>
            <button class="btn btn-danger" id="cancel">
                Hủy bỏ
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<script>
    tinymce.init({
        selector: 'textarea#content',
        plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Admin',
        height: 300,
        mergetags_list: [{
                value: 'Admin',
                title: 'Admin'
            },
            {
                value: 'Email',
                title: 'Email'
            },
        ],
        ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
            "See docs to implement AI Assistant"))
    });
    tinymce.init({
        selector: 'textarea#en_content',
        plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Admin',
        height: 300,
        mergetags_list: [{
                value: 'Admin',
                title: 'Admin'
            },
            {
                value: 'Email',
                title: 'Email'
            },
        ],
        ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
            "See docs to implement AI Assistant"))
    });

    $(document).ready(function() {
        try {
            $(document).on('submit', 'form', function(event) {
                // Prevent default posting of form - put here to work in case of errors
                event.preventDefault();

                var data = getBlogInfor();

                request = $.ajax({
                    url: "blog/save",
                    type: "post",
                    data: data
                });

                // Callback handler that will be called on success
                request.done(function(response, textStatus, jqXHR) {
                    // Log a message to the console
                    $.alert({
                        title: "Thông báo!",
                        content: "Lưu thành công",
                        buttons: {
                            OK: function() {
                                console.log('Save success to menu post')
                                location.href = "/personal-itinerary?menu=destination";
                            },
                        },
                    });
                });

                // Callback handler that will be called on failure
                request.fail(function(jqXHR, textStatus, errorThrown) {
                    // Log the error to the console
                    $.alert({
                        title: "Thông báo!",
                        content: "Lỗi hệ thống",
                        buttons: {
                            OK: function() {
                                console.log('Save error to menu post')
                                location.href = "/personal-itinerary?menu=post";
                            },
                        },
                    });
                });

            });
            $(document).on('click', '#cancel', function() {
                console.log('Click cancel to menu post')
                location.href = '/personal-itinerary?menu=post';
            });
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });


    function getBlogInfor() {
        try {
            return attr = {
                'mode': $("#mode").val(),
                'attr_id': $("#attr-id").val(),
                'title': $("#title").val(),
                'content': tinyMCE.get('content').getContent(),
                'en_title': $("#en_title").val(),
                'en_content': tinyMCE.get('en_content').getContent(),
            };
        } catch (e) {
            console.log('getBlogContent: ' + e.message);
        }
    }
</script>
