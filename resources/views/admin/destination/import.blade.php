<div class="manage-title">
    <h3>Nhập file địa điểm</h3>
</div>
<div class="update-container">
    <form action="{{ route('upload-destination') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="body-content">
            <label for="excel" class="drop-container" id="dropcontainer">
                <span class="drop-title">Drop files here</span>
                or
                <input type="file" id="excel" name="excel"
                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
            </label>
        </div>
        <div class="upload-area">
            <ion-icon name="paper-plane-outline"></ion-icon>
            <button class="btn btn-success import-button" type="submit" name="upload">Tải tệp lên</button>
        </div>
    </form>
</div>
