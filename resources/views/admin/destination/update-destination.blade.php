<div class="manage-title">
    <h3>Quản lý địa điểm</h3>
</div>
<div class="update-container">
    <div class="body-content">
        <div class="formm">
            <form enctype="multipart/form-data" method="post" action="{{ route('save-attraction') }}">
                @csrf
                <div class="form-group">
                    <label>Ảnh</label>
                    <input type="text" class="form-control" id="img_id" name="img_id" style="display: none;"
                        value="{{ $data['img_id'] ?? '' }}">
                    <label class="picture" for="picture__input" tabIndex="0">
                        <span class="picture__image">
                            @if (!empty($data['img_link']))
                                <img id="picture__image__preview" src="{{ asset('image/' . $data['img_link']) }}"
                                    class="picture__img">
                            @else
                                <img id="picture__image__preview" class="picture__img">
                            @endif
                        </span>
                    </label>

                    <input type="file" name="picture__input" id="picture__input"
                        @if (empty($data['img_id'])) required @endif>
                    @error('picture__input')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Ảnh bìa</label>
                    <label class="picture" id="cover-picture-label" for="cover_picture__input" tabIndex="0">
                        <span class="cover_picture__image">
                            @if (!empty($data['cover_img_link']))
                                <img id="cover__picture__image__preview"
                                    src="{{ asset('image/' . $data['cover_img_link']) }}" class="cover_picture__img">
                            @else
                                <img id="cover__picture__image__preview" class="cover_picture__img">
                            @endif
                        </span>
                    </label>

                    <input type="text" class="form-control" id="cover_img_id" name="cover_img_id"
                        style="display: none;" value="{{ $data['cover_img_id'] ?? '' }}">
                    <input type="file" name="cover_picture__input" id="cover_picture__input"
                        @if (empty($data['cover_img_id'])) required @endif>
                </div>
                <div class="form-group">
                    <label>Tên địa điểm</label>
                    <input type="text" class="form-control" name="mode" id="mode" value="{{ $data['mode'] }}"
                        style="display: none;">
                    <input type="text" class="form-control" name="attr_id" id="attr_id"
                        value="{{ $data['attr_id'] }}" style="display: none;">
                    <input type="text" class="form-control" name="attr_name" id="attr_name"
                        value="{{ $data['attr_name'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Attraction name</label>
                    <input type="text" class="form-control" name="en_attr_name" id="en_attr_name"
                        value="{{ $data['en_attr_name'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <input type="text" class="form-control" name="attr_address" id="attr_address"
                        value="{{ $data['attr_address'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" name="en_attr_address" id="en_attr_address"
                        value="{{ $data['en_attr_address'] ?? '' }}">
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Tọa độ (Định dạng: Kinh độ, vĩ độ | Lat, long)</label>
                        <input type="text" class="form-control" name="attr_location" id="attr_location"
                            placeholder="16.644, 108.1234"
                            value="{{ $data['lat'] && $data['long'] ? $data['lat'] . ',' . $data['long'] : '' }}">
                    </div>
                    <div class="col-md-6">
                        <label>Hiển thị trên trang chủ</label>
                        <select class="form-control" name="display" id="display">
                            <option value="1" @if ($data['display'] == 1) selected @endif>Hiển thị</option>
                            <option value="0" @if ($data['display'] == 0) selected @endif>Không hiển thị
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Nhãn</label>
                    <select class="form-control" name="tag" id="tag">
                        @foreach ($data['tags'] as $tag)
                            <option value="{{ $tag->tag_id }}" @if ($data['tag_id'] == $tag->tag_id) selected @endif>
                                {{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="start-date">Giờ mở cửa</label>
                        <div class="input-group date opening_time_start">
                            <input type="text" class="form-control time" name="opening_time_start"
                                id="opening_time_start" value="{{ $data['opening_time_start'] ?? '' }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="start-date">Giờ đóng cửa</label>
                        <div class="input-group date opening_time_end">
                            <input type="text" class="form-control time" name="opening_time_end"
                                id="opening_time_end" value="{{ $data['opening_time_end'] ?? '' }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Thời gian tham quan</label>
                    <input type="text" class="form-control time" name="attr_duration" id="attr_duration"
                        value="{{ $data['attr_duration'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Thời gian tham quan tốt nhất</label>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="start-date">Từ </label>
                        <div class="input-group date best_time_start">
                            <input type="text" class="form-control time" name="best_time_start"
                                id="best_time_start" value="{{ $data['best_time_start'] ?? '' }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="start-date">Đến </label>
                        <div class="input-group date best_time_end">
                            <input type="text" class="form-control time" name="best_time_end" id="best_time_end"
                                value="{{ $data['best_time_end'] ?? '' }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Giá (VNĐ)</label>
                    <input type="text" class="form-control" name="price" id="price"
                        value="{{ $data['price'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label>Mô tả chi tiết</label>
                    <textarea class="form-control" row="12" name="detail" id="detail">
                        {{ $data['detail'] ?? '' }}
                    </textarea>
                </div>
                <div class="form-group">
                    <label>Detail</label>
                    <textarea class="form-control" row="12" name="en_detail" id="en_detail">
                        {{ $data['en_detail'] ?? '' }}
                    </textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">
                        @if ($data['mode'] == 'I')
                            Thêm mới
                        @else
                            Cập nhật
                        @endif
                    </button>
                    <button class="btn btn-danger" id="cancel">
                        Hủy bỏ
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var regex = /:/gi;

    $(document).ready(function() {
        try {
            $(".time").each(function() {
                var time = $(this).text().replace(/\s/g, "");
                $(this).text([time.slice(0, 2), ":", time.slice(2)].join(""));
            });

            $("#opening_time_start").datetimepicker({
                format: "HH:mm",
            });
            $("#opening_time_end").datetimepicker({
                format: "HH:mm",
            });
            $("#best_time_start").datetimepicker({
                format: "HH:mm",
            });
            $("#best_time_end").datetimepicker({
                format: "HH:mm",
            });
            $("#attr_duration").datetimepicker({
                format: "HH:mm",
            });

            var detail = document.getElementById("detail");
            var en_detail = document.getElementById("en_detail");
            detail.value = detail.value.trim();
            en_detail.value = en_detail.value.trim();

            $(document).on('click', '#save-attr', function() {
                save();
            });
            $(document).on('click', '#cancel', function() {
                location.href = '/personal-itinerary?menu=destination';
            });

            const checkCoverImage = document.getElementById("cover_img_id");
            if (checkCoverImage.value != "") {
                const element = document.getElementById("cover-picture-label");
                element.classList.add("cover-picture");
            }
            const inputFile = document.querySelector("#picture__input");

            inputFile.addEventListener("change", function(e) {
                const inputTarget = e.target;
                const file = inputTarget.files[0];

                if (file) {
                    const reader = new FileReader();

                    reader.addEventListener("load", function(e) {
                        const readerTarget = e.target;

                        const img = document.getElementById("picture__image__preview");
                        img.src = readerTarget.result;
                        img.classList.add("picture__img");
                    });
                    reader.readAsDataURL(file);
                }
            });

            const coverInputFile = document.querySelector("#cover_picture__input");
            // const coverPictureImage = document.querySelector(".cover_picture__image");
            // const coverPictureImageTxt = "Choose an image";
            // coverPictureImage.innerHTML = pictureImageTxt;

            coverInputFile.addEventListener("change", function(e) {
                const inputTarget = e.target;
                const file = inputTarget.files[0];

                if (file) {
                    const reader = new FileReader();

                    reader.addEventListener("load", function(e) {
                        const readerTarget = e.target;

                        const img = document.getElementById("cover__picture__image__preview");
                        img.src = readerTarget.result;
                        img.classList.add("cover_picture__img");
                        const element = document.getElementById("cover-picture-label");
                        element.classList.add("cover-picture");
                    });
                    reader.readAsDataURL(file);
                }
            });
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });

    function getAttrInfor() {
        try {
            return attr = {
                'attr_id': $("#attr-id").val(),
                'attr_name': $("#attr-name").val(),
                'attr_address': $("#attr-address").val(),
                'en_attr_name': $("#en_attr-name").val(),
                'en_attr_address': $("#en_attr-address").val(),
                'attr_lat': $("#attr-location").val().split(',')[0].trim(),
                'attr_long': $("#attr-location").val().split(',')[1].trim(),
                'opening_time_start': $("#opening_time_start").val().replace(regex, ''),
                'opening_time_end': $("#opening_time_end").val().replace(regex, ''),
                'attr_duration': $("#attr_duration").val().replace(regex, ''),
                'best_time_start': $("#best_time_start").val().replace(regex, ''),
                'best_time_end': $("#best_time_end").val().replace(regex, ''),
                'price': $("#price").val(),
                'detail': $("#detail").val(),
                'en_detail': $("#en_detail").val(),
                'tag_id': $("#tag").val(),
                'img_id': $("#img_id").val(),
                'cover_img_id': $("#cover_img_id").val(),
            };
        } catch (e) {
            console.log('getAttractionInfo: ' + e.message);
        }
    }
</script>
