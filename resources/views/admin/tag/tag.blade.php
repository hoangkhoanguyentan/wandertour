@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
@endsection

<div class="manage-title">
    <h3>Quản lý nhãn</h3>
    <button onclick="addTag()" class="btn btn-primary add-new">Thêm mới</button>
</div>
<table class="table">
    <thead>
        <tr>
            <th class="tag-no">STT</th>
            <th class="tag-name">Tên</th>
            <th class="tag-name">Tên (English)</th>
            <th class="tag-action">Hành động</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['tag'] as $i => $tag)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $tag->name ?? '' }}</td>
                <td>{{ $tag->en_name ?? '' }}</td>
                <td>
                    <a onclick="editTag({{ $tag->tag_id }})" class="btn btn-warning">Edit</a>
                    <a onclick="confirmDeleteTag({{ $tag->tag_id }})" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
