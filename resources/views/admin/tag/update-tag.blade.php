@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
@endsection

<div class="manage-title">
    <h3>Thêm mới nhãn</h3>
</div>
<div class="update-container">
    <div class="body-content">
        <div class="formm">
            <form>
                <div class="form-group">
                    <label>Tên nhãn</label>
                    <input type="text" class="form-control" id="mode" value="{{ $data['mode'] }}"
                        style="display: none;">
                    <input type="text" class="form-control" id="tag-id" value="{{ $data['tag_id'] ?? '' }}"
                        style="display: none;">
                    <input type="text" class="form-control" name="tag-name" id="tag-name"
                        value="{{ $data['name'] }}">
                </div>
                <div class="form-group">
                    <label>Tag name</label>
                    <input type="text" class="form-control" name="en_tag-name" id="en_tag-name"
                        value="{{ $data['en_name'] }}">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" id="save-tag">
                        @if ($data['mode'] == 'I')
                            Thêm mới
                        @else
                            Cập nhật
                        @endif
                    </button>
                    <button class="btn btn-danger" id="cancel">
                        Hủy bỏ
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        try {
            $(document).on('click', '#save-tag', function() {
                save();
            });
            $(document).on('click', '#cancel', function() {
                location.href = '/personal-itinerary?menu=tag';
            });
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });

    function getTagInfor() {
        try {
            return tag = {
                'tag_id': $("#tag-id").val(),
                'name': $("#tag-name").val(),
                'en_name': $("#en_tag-name").val(),
            };
        } catch (e) {
            console.log('getTagInfo: ' + e.message);
        }
    }

    function save() {
        try {
            var mode = $("#mode").val();
            var tag_id = $("#tag-id").val();
            var name = $("#tag-name").val();
            var en_name = $("#en_tag-name").val();
            if (name == '' && en_name == '') {
                $.alert({
                    title: 'Thông báo!',
                    content: 'Tên nhãn không được để trống',
                    buttons: {
                        OK: function() {
                            location.href = '/personal-itinerary?menu=tag';
                        }
                    }
                });
                return;
            }
            var tag = {
                tag_id,
                name,
                en_name,
            };
            $.ajax({
                type: 'POST',
                url: '/update-tag/save',
                data: {
                    tag,
                    mode,
                },
                success: function(res) {
                    $.alert({
                        title: 'Thông báo!',
                        content: 'Lưu thành công',
                        buttons: {
                            OK: function() {
                                location.href = '/personal-itinerary?menu=tag';
                            }
                        }
                    });
                }
            });
        } catch (e) {
            window.alert("save " + e);
        }
    }
</script>
