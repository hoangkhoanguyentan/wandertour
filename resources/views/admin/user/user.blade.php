@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
@endsection

<div class="manage-title">
    <h3>Quản lý người dùng</h3>
    <button onclick="addUser()" class="btn btn-primary add-new">Thêm mới</button>
</div>
<table class="table">
    <thead>
        <tr>
            <th class="tag-no">STT</th>
            <th class="tag-name">Tên tài khoản</th>
            <th class="tag-action">Hành động</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['user'] as $i => $user)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $user->username ?? '' }}</td>
                <td>
                    <a onclick="editUser({{ $user->user_id }})" class="btn btn-warning">Edit</a>
                    <a onclick="confirmDeleteUser({{ $user->user_id }})" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
