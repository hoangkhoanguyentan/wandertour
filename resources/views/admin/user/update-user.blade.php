@section('assets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app/admin.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link href="{{ asset('css/library/select2.css') }}" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="{{ asset('js/library/select2.js') }}"></script>
    <script>
        var selectedInterested = {!! json_encode($data['selectedInterested']) !!};
    </script>
@endsection

<div class="manage-title">
    <h3>Thêm mới người dùng</h3>
</div>
<div class="update-container update-user-container">
    <div class="body-content">
        <div class="user-form">
            <div class="form-group">
                <label>Tên người dùng</label>
                <input type="text" class="form-control" id="mode" name="mode" value="{{ $data['mode'] }}"
                    style="display: none;">
                <input type="text" class="form-control" id="user_id" name="user_id"
                    value="{{ $data['user_id'] ?? '' }}" style="display: none;">
                <input type="text" class="form-control" id="username" name="username"
                    value="{{ $data['username'] }}">
            </div>
            <div class="form-group">
                <label>Mật khẩu</label>
                <input type="text" class="form-control" id="password" name="password"
                    value="{{ $data['password'] }}">
            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ $data['email'] }}">
            </div>
            <div class="form-group">
                <label>Số điện thoại</label>
                <input type="text" class="form-control" id="phone_number" name="phone_number"
                    value="{{ $data['phone_number'] }}">
            </div>
            <div class="form-group">
                <label>Tên đầy đủ</label>
                <input type="text" class="form-control" id="full_name" name="full_name"
                    value="{{ $data['full_name'] }}">
            </div>
            <div class="form-group">
                <label>Giới tính</label>
                <input type="text" class="form-control" id="sex" name="sex" value="{{ $data['sex'] }}">
            </div>
            <div class="form-group">
                <div class="row">
                    <label>Sở thích</label>

                </div>
                <div class="row">
                    <select class="selectpicker" multiple>
                        @forelse($data['interested'] as $item)
                            <option value="{{ $item->name }}"
                                {{ in_array($item->name, $data['selectedInterested']) ? 'selected' : '' }}>
                                {{ $item->name }}
                            </option>
                        @empty
                            <p>There are no interests yet!</p>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label>Năm sinh</label>
                <input type="text" class="form-control" id="year_of_birth" name="year_of_birth"
                    value="{{ $data['year_of_birth'] }}">
            </div>
            <div class="form-group">
                <label>Nghề nghiệp</label>
                <input type="text" class="form-control" id="occupation" name="occupation"
                    value="{{ $data['occupation'] }}">
            </div>
            <div class="form-group">
                <button class="btn btn-success" id="save-user">
                    @if ($data['mode'] == 'I')
                        Thêm mới
                    @else
                        Cập nhật
                    @endif
                </button>
                <button class="btn btn-danger" id="cancel"
                    data-href="{{ URL::to('/personal-itinerary?menu=user') }}">
                    Hủy bỏ
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        try {
            console.log('init');
            $(document).on('click', '#save-user', function() {
                save();
            });
            $(document).on('click', '#cancel', function() {
                window.location.href = $(this).data('href');
            });
            $('select').select2();
        } catch (e) {
            window.alert("initEvent " + e);
        }
    });

    function getUserInfor() {
        try {
            return user = {
                'user_id': $("#user_id").val(),
                'username': $("#username").val(),
                'password': $("#password").val(),
                'email': $("#email").val(),
                'phone_number': $("#phone_number").val(),
                'full_name': $("#full_name").val(),
                'sex': $("#sex").val(),
                'year_of_birth': $("#year_of_birth").val(),
                'occupation': $("#occupation").val(),
                'interested': $("select").val(),
            };
        } catch (e) {
            console.log('getuserInfo: ' + e.message);
        }
    }

    function save() {
        try {
            var mode = $("#mode").val();
            if (username == '' || password == '') {
                $.alert({
                    title: 'Thông báo!',
                    content: 'Tên hoặc mật khẩu không được để trống',
                    buttons: {
                        OK: function() {
                            location.href = '/personal-itinerary?menu=user';
                        }
                    }
                });
                return;
            }
            var userData = getUserInfor();
            $.ajax({
                type: 'POST',
                url: '/update-user/save',
                data: {
                    user: userData,
                    mode,
                },
                success: function(res) {
                    $.alert({
                        title: 'Thông báo!',
                        content: 'Lưu thành công',
                        buttons: {
                            OK: function() {
                                window.location.href = '/personal-itinerary?menu=user';

                            }
                        }
                    });
                    window.location.href = '/personal-itinerary?menu=user';
                },
                error: function(xhr, status, error) {
                    $.alert({
                        title: 'Thông báo!',
                        content: 'Có lỗi xảy ra khi lưu, vui lòng kiểm tra lại dữ liệu và thử lại',
                    });
                    return;
                }

            });
        } catch (e) {
            window.alert("save " + e);
        }
    }
</script>
