<?php

return [
    'Destination' => 'Địa điểm',
    'Gastronomies' => 'Ẩm thực',
    'Itinerary' => 'Lịch trình',
    'Account' => 'Tài khoản',
    'Logout' => 'Đăng xuất',
    'Plan and go' => 'WANDERING TOUR',
    'We make planning your trip easier' => 'Chúng tôi giúp lên kế hoạch cho chuyến đi của bạn một cách dễ dàng hơn',
    'ChooseTime' => 'Thời gian',
    'ChooseInterests' => 'Sở thích',

    'Preferences' => 'Lựa chọn sở thích',
    'CustomizeDestinations' => 'Tùy chỉnh điểm đến',
    'EnjoyTrip' => 'Tận hưởng chuyến đi',
    'Select your preferred itineraries.' => 'Chọn lịch trình ưa thích của bạn.',
    'Customize your chosen destinations or create your own.' => 'Tùy chỉnh các điểm đến bạn đã chọn hoặc tạo lịch trình riêng của bạn.',
    'Enjoy your trip to the fullest by customizing itineraries or creating your own.' => 'Tận hưởng chuyến đi của bạn đến tận cùng bằng cách tùy chỉnh lịch trình hoặc tạo lịch trình riêng của bạn.',

    'AttractiveDestinations' => 'Những điểm đến hấp dẫn',
    'LocalCuisine' => 'Huong vị đậm chất địa phương',
    'RecommendedItinerary' => 'Lịch trình đề xuất',
    'NoRecommendedItinerary'=> 'Không có lịch trình đề xuất!',

    'Amount' => 'Số tiền',
    'Duration' => 'Thời gian',
    'days' => 'ngày',
    'ViewItinerary' => 'Xem lịch trình',
    'FindItinerary' => 'Tìm lịch trình',
    'Danang introduce' => 'Đà Nẵng, thành phố tuyệt vời nằm bên bờ biển miền Trung Việt Nam, không chỉ là điểm đến lý tưởng cho những chuyến du lịch, mà còn là nơi hội tụ giữa văn hóa truyền thống và sự hiện đại năng động.',
    'Links' => 'Liên kết',
    'Home' => 'Trang chủ',
    'Login' => 'Đăng nhập',
    'Signup' => 'Đăng ký',
    'Language' => 'Ngôn ngữ',
    'EmailAddress' => 'Địa chỉ email',
    'Subscribing' => 'Đừng bỏ lỡ việc đăng ký theo dõi các cập nhật mới của chúng tôi, hãy điền thông tin vào mẫu trên.',
    'nopost' => 'Không có bài viết nào'
];
