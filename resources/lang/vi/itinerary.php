<?php

return [
    'home' => 'Trang chủ',
    'itinerary' => 'Lịch trình',
    'detail' => 'Chi tiết',
    'download' => 'Tải xuống',
    'like' => 'Thích',
    'from' => 'Từ ngày',
    'duration' => 'Số ngày',
    'destinations' => 'Số điểm đến',
    'destination' => 'địa điểm',
    'day' => 'ngày',
    'Day' => 'Ngày',
    'days' => 'Số ngày',
    'comments' => 'Bình luận',
    'rate' => 'Đánh giá',
    'why' => 'Vì sao chọn chúng tôi?',
    'over' => 'Hơn +300 địa điểm',
    'uniquetravel' => 'Địa điểm du lịch mang nét riêng',
    'lowcost' => 'Chi phí thấp và tiết kiệm',
    'bestdeal' => 'Mang đến những dịch vụ ưu đãi nhất',
    'excellent' => 'Hỗ trợ tuyệt vời',
    'prostaff' => 'Đội ngũ nhân viên hỗ trợ chuyên nghiệp',
    'needhelp' => 'Bạn cần hỗ trợ?',
    'needsp' => 'Bạn cần hỗ trợ trực tuyến hãy liên hệ với chúng tôi.',
    'itidetail' => 'Chi tiết lịch trình',
    'custom' => 'Tùy chỉnh lịch trình',

    'itiname' => 'Tên lịch trình',
    'start' => 'Ngày bắt đầu',
    'end' => 'Ngày kết thúc',
    'transport' => 'Phương tiện',
    'interests' => 'Sở thích',
    'people' => 'Số người',
    'hotelmotel' => 'Khách sạn/Nhà nghỉ/Homestay',
    'descall' => 'Điểm đến',
    'gascall' => 'Ẩm thực',
    'shopcall' => 'Mua sắm',
    'acccall' => 'Lưu trú',
    'name' => 'Tên',

    'address' => 'Địa chỉ',
    'roomno' => 'Số phòng',
    'checkin' => 'Check in',
    'checkout' => 'Check out',
    'time' => 'Giờ',
    'bookingno' => 'Mã số booking',
    'saveiti' => 'Lưu lịch trình',
    'open' => 'Mở cửa',
    'close' => 'Đóng cửa',
    'custombreadcrumb' => 'Tùy chỉnh',

    'transportation' => 'Phương tiện di chuyển',
    'tranfrom' => 'Từ',
    'tranto' => 'Đến',
    'departure' => 'Khởi hành',
    'return' => 'Khứ hồi',
    'flightno' => 'Chuyến số',
    'save' => 'Lưu',
    'cancel' => 'Hủy',
    'dncity' => 'Thành phố Đà Nẵng',
    'vn' => 'Việt Nam',
    'touratt' => 'Địa điểm du lịch',
    'free' => 'Miễn phí'
];
