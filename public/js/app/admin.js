var regex = /:/gi;

$(document).ready(function () {
    try {
        if (
            selectedMenu === "" ||
            selectedMenu === undefined ||
            selectedMenu === null
        ) {
            loadBlade("destination");
        } else {
            console.log(selectedMenu);
            loadBlade(selectedMenu);
        }
        // Add click event to menu items
        $(".menu-item").click(function (e) {
            e.preventDefault();

            // Remove 'active' class from all menu items
            $(".menu-item").removeClass("active");

            // Add 'active' class to the clicked menu item
            $(this).addClass("active");

            // Load the corresponding blade using JavaScript
            loadBlade($(this).data("target"));
        });
        $(".time").each(function () {
            var time = $(this).text().replace(/\s/g, "");
            $(this).text([time.slice(0, 2), ":", time.slice(2)].join(""));
        });
    } catch (error) {
        window.alert("document " + error);
    }
});

// Function to load blade using JavaScript
function loadBlade(target) {
    // Construct the path to your blade view
    $.get("/load-blade/" + target, function (response) {
        var targetElement = document.querySelector(
            '[data-target="' + target + '"]'
        );
        // Remove 'active' class from all menu items
        $(".menu-item").removeClass("active");

        // Add 'active' class to the clicked menu item
        targetElement.classList.add("active");

        $("#blade-container").html(response);
    });
}
function addAttraction() {
    $.get("/update-destination?mode=I", function (response) {
        $("#blade-container").html(response);
    });
}
function importAttraction() {
    $.get("/import-destination", function (response) {
        $("#blade-container").html(response);
    });
}
function editAttraction(attrId) {
    $.get("/update-destination/" + attrId + "?mode=U", function (response) {
        $("#blade-container").html(response);
    });
}
function editBlog(attrId) {
    $.get("/blog/" + attrId, function (response) {
        $("#blade-container").html(response);
    });
}

function confirmDeleteAttraction(attrId) {
    if (confirm("Bạn có chắc muốn xóa mục này?")) {
        // Nếu người dùng xác nhận muốn xóa, bạn có thể chuyển đến URL xóa hoặc thực hiện các thao tác cần thiết ở đây.
        $.ajax({
            type: "GET",
            url: "/delete-destination/" + attrId,
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Xóa thành công",
                    buttons: {
                        OK: function () {
                            location.href =
                                "/personal-itinerary?menu=destination";
                        },
                    },
                });
            },
        });
    }
}

function addTag() {
    $.get("/update-tag?mode=I", function (response) {
        $("#blade-container").html(response);
    });
}

function editTag(tagId) {
    $.get("/update-tag/" + tagId + "?mode=U", function (response) {
        $("#blade-container").html(response);
    });
}

function confirmDeleteTag(tagId) {
    if (confirm("Bạn có chắc muốn xóa mục này?")) {
        // Nếu người dùng xác nhận muốn xóa, bạn có thể chuyển đến URL xóa hoặc thực hiện các thao tác cần thiết ở đây.
        $.ajax({
            type: "GET",
            url: "/delete-tag/" + tagId,
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Xóa thành công",
                    buttons: {
                        OK: function () {
                            location.href = "/personal-itinerary?menu=tag";
                        },
                    },
                });
            },
        });
    }
}

function confirmDeleteTour(tourId) {
    if (confirm("Bạn có chắc muốn xóa mục này?")) {
        // Nếu người dùng xác nhận muốn xóa, bạn có thể chuyển đến URL xóa hoặc thực hiện các thao tác cần thiết ở đây.
        $.ajax({
            type: "GET",
            url: "/delete-tour/" + tourId,
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Xóa thành công",
                    buttons: {
                        OK: function () {
                            location.href = "/personal-itinerary?menu=tour";
                        },
                    },
                });
            },
        });
    }
}

function addPost() {
    $.get("/update-post?mode=I", function (response) {
        $("#blade-container").html(response);
    });
}

function editPost(postId) {
    $.get("/update-post/" + postId + "?mode=U", function (response) {
        $("#blade-container").html(response);
    });
}

function confirmDeletePost(postId) {
    if (confirm("Bạn có chắc muốn xóa mục này?")) {
        // Nếu người dùng xác nhận muốn xóa, bạn có thể chuyển đến URL xóa hoặc thực hiện các thao tác cần thiết ở đây.
        $.ajax({
            type: "GET",
            url: "/delete-post/" + postId,
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Xóa thành công",
                    buttons: {
                        OK: function () {
                            console.log("delete ok to menu post");
                            location.href = "/personal-itinerary?menu=post";
                        },
                    },
                });
            },
        });
    }
}

function addUser() {
    $.get("/update-user?mode=I", function (response) {
        $("#blade-container").html(response);
    });
}

function editUser(userId) {
    $.get("/update-user/" + userId + "?mode=U", function (response) {
        $("#blade-container").html(response);
    });
}

function confirmDeleteUser(userId) {
    if (confirm("Bạn có chắc muốn xóa mục này?")) {
        // Nếu người dùng xác nhận muốn xóa, bạn có thể chuyển đến URL xóa hoặc thực hiện các thao tác cần thiết ở đây.
        $.ajax({
            type: "GET",
            url: "/delete-user/" + userId,
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Xóa thành công",
                    buttons: {
                        OK: function () {
                            location.href = "/personal-itinerary?menu=user";
                        },
                    },
                });
            },
        });
    }
}
