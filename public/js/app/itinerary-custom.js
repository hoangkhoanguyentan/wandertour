var transport = {};
var accommodation = {};
var tourInfo = {};
var tourTag;
var tourDetail = [];
var directionsService;
var regex = /:/gi;

$(document).ready(function () {
    try {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        initialize();
        initEvent();
        $("#tour-name").focus();
    } catch (e) {
        window.alert("document " + e);
    }
});
// function to init gg map api
function initMap() {
    // draw route
    directionsService = new google.maps.DirectionsService();
    //
    initTimeAndDistance();
}
// init time and distance on view
function initTimeAndDistance() {
    for (var i = 0; i <= duration; i++) {
        var lstDayOfTourOnMapOnAdd = [];
        $("#lst_place_date_" + i)
            .find("li.li-parent")
            .each(function () {
                lstDayOfTourOnMapOnAdd.push({
                    location: $(this).attr("data-geo"),
                });
            });

        $("#lst_place_date_" + i + " li.li-parent:last-child")
            .children(".calculater-route-info")
            .html("");
        if (lstDayOfTourOnMapOnAdd.length > 0) {
            //calc distance
            calculateAndDisplayRoute(lstDayOfTourOnMapOnAdd, i, false);
        }
    }
}
// initialize object
function initialize() {
    try {
        $("#interested").select2();
        initDateTimePicker();
        var ps = new PerfectScrollbar(".lst-item-form");
        // var ps = new PerfectScrollbar('');
        var html = Mustache.render($("#item_tour_detail").html(), {
            Geo: 1,
            DataNation: 1,
            Day: 1,
            PlaceInfo: 1,
            Image: 1,
            Time: 11,
            PlaceName: 11,
            TimeOpen1: 22,
            TimeOpen: 33,
            TimeClose: 44,
            VisitTime: 55,
        });
        // console.log(html);
        $(".list-attraction-bar").waypoint(
            function (direction) {
                if (direction === "down") {
                    $(".list-attraction-bar").addClass(
                        "list-attraction-bar-sticky"
                    );
                } else {
                    $(".list-attraction-bar").removeClass(
                        "list-attraction-bar-sticky"
                    );
                }
            },
            {
                offset: "50px",
            }
        );
        $(document).on("click", "#save-tour", function () {
            save();
        });
    } catch (e) {
        window.alert("initialize " + e);
    }
}
// initialize object event
function initEvent() {
    try {
        $(".time").each(function () {
            var time = $(this).text().replace(/\s/g, "");
            $(this).text([time.slice(0, 2), ":", time.slice(2)].join(""));
        });
        $("#btn-transport").click(function () {
            $("#transport-popup").modal("show");
        });
        $("#btn-accommodation").click(function () {
            $("#accommodation-popup").modal("show");
        });
        $("#btn-transport-save").click(function () {
            getInfoTransport();
            // console.log(transport);
            $("#transport-popup").modal("hide");
        });
        $("#btn-accommodation-save").click(function () {
            getInfoAccommodation();
            $("#accommodation-popup").modal("hide");
        });
        $(document).on("keyup", ".input-findname", function () {
            searchAttraction();
            console.log($("input[name='search-attraction']").val());
        });
        //init drop list item place search right
        Sortable.create(document.getElementById("tab-panel-1"), {
            sort: false,
            group: {
                name: "place_item",
                pull: "clone",
                put: false,
            },
            animation: 150,
            scroll: true,
        });
        //init drop list item place search right
        // Sortable.create(document.getElementById("lst_place_date_1"), {
        //     sort: true,
        //     group: {
        //         name: 'place_item',
        //         pull: false,
        //         put: true
        //     },
        //     animation: 150,
        //     scroll: true
        // });

        $(document).on("change", "#hotel", function () {
            $("#hotel-name").val($(this).val());
        });
        $(document).on("change", "#hotel-name", function () {
            $("#hotel").val($(this).val());
        });

        setTimeout(initDatePlaceSortDrop(), 200);

        //Xoa dia diem trong ngay
        $(document).on("click", ".remove-place", function () {
            var $this = $(this);
            // bootbox.confirm({
            //     title: "Xác nhận",
            //     message: "Bạn muốn xóa địa điểm này khỏi lịch trình?",
            //     buttons: {
            //         cancel: {
            //             label: '<i class="fa fa-times"></i> Đóng'
            //         },
            //         confirm: {
            //             label: '<i class="fa fa-check"></i> Đồng ý'
            //         }
            //     },
            //     callback: function (result) {
            //         if(result) {
            //             var dayId = $this.closest('.lst-date-item').attr("date-no");
            //             $this.closest('li').remove();
            //             var checkLength = $("#lst_place_date_" + dayId).find("li").length;
            //             if(checkLength == 0) {
            //                 $('#edit-tour-' + dayId).find(".place-tour-day").trigger("click");
            //             }
            //             var lstDayOfTourOnMapOnAdd = [];
            //             $('#lst_place_date_' + dayId).find("li.li-parent").each(function () {
            //                 lstDayOfTourOnMapOnAdd.push({ "location": $(this).attr("data-geo") });
            //             });
            //             $('#lst_place_date_'+ dayId +' li.li-parent:last-child').children(".calculater-route-info").html('');
            //
            //             if(lstDayOfTourOnMapOnAdd.length > 0) {
            //                 //calc distance
            //                 calculateAndDisplayRoute(lstDayOfTourOnMapOnAdd, dayId, true);
            //             }
            //         }
            //     }
            // });
            $.confirm({
                title: "Xác nhận!",
                content: "Bạn muốn xóa địa điểm này khỏi lịch trình?",
                buttons: {
                    "Xác nhận": {
                        btnClass: "btn-blue",
                        action: function () {
                            var dayId = $this
                                .closest(".lst-date-item")
                                .attr("date-no");
                            $this.closest("li").remove();
                            var checkLength = $(
                                "#lst_place_date_" + dayId
                            ).find("li").length;
                            if (checkLength == 0) {
                                $("#edit-tour-" + dayId)
                                    .find(".place-tour-day")
                                    .trigger("click");
                            }
                            var lstDayOfTourOnMapOnAdd = [];
                            $("#lst_place_date_" + dayId)
                                .find("li.li-parent")
                                .each(function () {
                                    lstDayOfTourOnMapOnAdd.push({
                                        location: $(this).attr("data-geo"),
                                    });
                                });
                            $(
                                "#lst_place_date_" +
                                    dayId +
                                    " li.li-parent:last-child"
                            )
                                .children(".calculater-route-info")
                                .html("");

                            if (lstDayOfTourOnMapOnAdd.length > 0) {
                                //calc distance
                                calculateAndDisplayRoute(
                                    lstDayOfTourOnMapOnAdd,
                                    dayId,
                                    true
                                );
                            }
                        },
                    },
                    Hủy: function () {},
                },
            });
        });
    } catch (e) {
        window.alert("initEvent " + e);
    }
}
// int date time picker
function initDateTimePicker() {
    $(".end-date-class").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $(".start-date-class").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $(".transport-start-date").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $(".transport-end-date").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $("#transport-start-time").datetimepicker({
        format: "HH:mm",
    });
    $("#transport-end-time").datetimepicker({
        format: "HH:mm",
    });
    $(".check-in-date").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $(".check-out-date").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $("#check-in-time").datetimepicker({
        format: "HH:mm",
    });
    $("#check-out-time").datetimepicker({
        format: "HH:mm",
    });
}
// search attraction
function searchAttraction() {
    try {
        $.ajax({
            type: "POST",
            url: "/itinerary-custom/search",
            data: {
                attraction_nm: $("input[name='search-attraction']").val(),
            },
            success: function (data) {
                $("#tab-panel-1").html(data);
            },
        });
    } catch (e) {
        console.log(e.message);
    }
}
function findAttractionByTag(tag) {
    try {
        $.ajax({
            type: "POST",
            url: "/itinerary-custom/searchByTag",
            data: {
                attraction_tag: tag,
            },
            success: function (data) {
                $("#tab-panel-1").html(data);
            },
        });
    } catch (e) {
        console.log(e.message);
    }
}
// init drop list date
function initDatePlaceSortDrop() {
    for (var i = 1; i <= duration; i++) {
        Sortable.create(document.getElementById("lst_place_date_" + i), {
            sort: true,
            group: {
                name: "place_item",
                pull: false,
                put: true,
            },
            scroll: true,
            animation: 250,
            onAdd: function (evt) {
                var newIDItem =
                    evt.item.attributes.getNamedItem("data-id").value;
                var imgSrc = evt.item.childNodes[1].children[0].src;
                var placeName = evt.item.children[1].children[0].textContent;
                var areaDay = evt.target.attributes
                    .getNamedItem("id")
                    .value.split("_")[3];
                // var data_tour = evt.item.attributes.getNamedItem("data-tour").value;
                var data_geo =
                    evt.item.attributes.getNamedItem("data-geo").value;
                var timeopen =
                    evt.item.attributes.getNamedItem("data-time-open").value;
                var timeclose =
                    evt.item.attributes.getNamedItem("data-time-close").value;
                var visittime =
                    evt.item.attributes.getNamedItem("data-visit-time").value;
                var arr = {
                    Geo: data_geo,
                    Day: areaDay,
                    PlaceInfo: newIDItem,
                    Image: imgSrc,
                    PlaceName: placeName,
                    TimeOpen: timeopen,
                    TimeClose: timeclose,
                    VisitTime: visittime,
                };

                var lstDayOfTourOnMapOnAdd = [];
                var arrayCheck = [];
                $("#lst_place_date_" + areaDay)
                    .find("li.li-parent")
                    .each(function () {
                        var itemAttr = $(this).attr("data-id");
                        if (itemAttr === newIDItem) {
                            arrayCheck.push(newIDItem);
                        }
                    });
                if (
                    evt.from.id === "tab-panel-1" ||
                    evt.from.id === "list_place_res" ||
                    evt.from.id === "list_place_hotel" ||
                    evt.from.id === "list_place_shop"
                ) {
                    if (arrayCheck.length > 0) {
                        var itemAdd = $(evt.item);
                        itemAdd.remove();
                        // bootbox.alert({
                        //     title: "Thông báo",
                        //     message: "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!",
                        // });
                        $.alert({
                            title: "Thông báo",
                            content:
                                "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!",
                        });
                    } else {
                        var timeopen1;
                        if (timeopen === "00:00") {
                            timeopen1 = "07:30";
                        } else {
                            timeopen1 = timeopen;
                        }
                        var html = Mustache.render(
                            $("#item_tour_detail").html(),
                            {
                                Geo: data_geo,
                                // DataNation: data_tour,
                                Day: areaDay,
                                PlaceInfo: newIDItem,
                                Image: imgSrc,
                                Time: timeopen1,
                                PlaceName: placeName,
                                TimeOpen1: timeopen1,
                                TimeOpen: timeopen1,
                                TimeClose: timeclose,
                                VisitTime: visittime,
                            }
                        );
                        evt.item.outerHTML = html;
                        $("#lst_place_date_" + areaDay)
                            .find("li.li-parent")
                            .each(function () {
                                lstDayOfTourOnMapOnAdd.push({
                                    location: $(this).attr("data-geo"),
                                });
                            });
                        calculateAndDisplayRoute(
                            lstDayOfTourOnMapOnAdd,
                            areaDay,
                            true
                        );
                        // Init all choose time
                        // initTimeAllPlace();
                    }
                }
            },
            onSort: function (evt) {
                var lstDayOfTourOnMapOnAdd = [];
                var areaDay = evt.to.attributes[1].value.split("_")[3];
                $("#lst_place_date_" + areaDay)
                    .find("li.li-parent")
                    .each(function () {
                        lstDayOfTourOnMapOnAdd.push({
                            location: $(this).attr("data-geo"),
                        });
                    });
                $("#lst_place_date_" + areaDay + " li.li-parent:last-child")
                    .children(".calculater-route-info")
                    .html("");
                if (lstDayOfTourOnMapOnAdd.length > 0) {
                    //calc distance
                    calculateAndDisplayRoute(
                        lstDayOfTourOnMapOnAdd,
                        areaDay,
                        true
                    );
                }
            },
        });
    }
}
// calculate route
function calculateAndDisplayRoute(lstDayOfTourOnMap, aui, isInit) {
    var waypts = [];
    for (var i = 1; i < lstDayOfTourOnMap.length - 1; i++) {
        waypts.push({
            location: lstDayOfTourOnMap[i].location,
            stopover: true,
        });
    }
    directionsService.route(
        {
            origin: lstDayOfTourOnMap[0].location,
            destination:
                lstDayOfTourOnMap[lstDayOfTourOnMap.length - 1].location,
            waypoints: waypts,
            travelMode: "DRIVING",
        },
        function (response, status) {
            if (status === "OK") {
                var templateHtml = $("#info_distance").html();
                var html = "";
                var checkLength = $("#edit-tour-" + aui).find("li").length;
                if (checkLength > 1) {
                    $("#edit-tour-" + aui)
                        .find("li:not(:last-child)")
                        .each(function (i) {
                            html = Mustache.render(templateHtml, {
                                DISTANCE:
                                    response.routes[0].legs[i].distance.text,
                                TIMERUN:
                                    response.routes[0].legs[i].duration.text,
                            });
                            $(this)
                                .children(".calculater-route-info")
                                .html(html);
                        });
                }
                if (isInit) {
                    var lengthResponse = response.routes["0"].legs.length;
                    var listCheck = [];

                    $("#edit-tour-" + aui)
                        .find("li.li-parent")
                        .each(function () {
                            listCheck.push($(this));
                        });
                    if (listCheck.length > 1) {
                        var currentTime = "";
                        for (var i = 0; i < lengthResponse; i++) {
                            if (i === 0) {
                                currentTime =
                                    listCheck[i][0].attributes[1].value; // lấy thời gian mở cửa địa điểm đầu tiên
                            } else {
                                currentTime = listCheck[i]
                                    .find(".change-time")
                                    .text(); // lấy thời gian mở cửa địa điểm tiếp theo
                            }
                            if (currentTime.indexOf(":") < 0) {
                                currentTime = [
                                    currentTime.slice(0, 2),
                                    ":",
                                    currentTime.slice(2),
                                ].join("");
                            }

                            var toptime = moment(
                                currentTime.split(":")[0] +
                                    ":" +
                                    currentTime.split(":")[1],
                                "HH:mm"
                            );
                            //
                            var time = moment(
                                currentTime.split(":")[0] +
                                    ":" +
                                    currentTime.split(":")[1],
                                "HH:mm"
                            );
                            //
                            var visitTime = moment(
                                listCheck[i][0].attributes[5].value.substring(
                                    0,
                                    2
                                ) +
                                    ":" +
                                    listCheck[
                                        i
                                    ][0].attributes[5].value.substring(2, 5),
                                "HH:mm"
                            );
                            var b = visitTime.clone().startOf("day");
                            //
                            response.routes[0].legs[i].duration.value =
                                parseInt(
                                    response.routes[0].legs[i].duration.value +
                                        visitTime.diff(b, "seconds")
                                );

                            //
                            time.add(
                                response.routes[0].legs[i].duration.value,
                                "s"
                            );
                            //
                            var timeCalc = moment(time, "HH:mm");
                            //
                            var openTime = moment(
                                listCheck[i + 1][0].attributes[1].value,
                                "HH:mm"
                            );
                            //
                            var closeTime = moment(
                                listCheck[i + 1][0].attributes[2].value,
                                "HH:mm"
                            );
                            // console.log(time.format("HH:mm"));
                            //
                            listCheck[i]
                                .find(".change-time")
                                .text(toptime.format("HH:mm"));
                            //
                            listCheck[i + 1]
                                .find(".change-time")
                                .text(time.format("HH:mm"));
                            //
                            if (
                                closeTime.isBefore(timeCalc) ||
                                timeCalc.isBefore(openTime)
                            ) {
                                listCheck[i + 1]
                                    .find(".time-error")
                                    .text(
                                        "Thời gian đến địa điểm đã đóng/ hoặc chưa mở cửa"
                                    );
                                // listCheck[i].find(".change-time").text(openTime.format("HH:mm"));
                            } else {
                                listCheck[i + 1].find(".time-error").text("");
                            }
                        }
                    }
                }
            } else {
                // bootbox.alert({
                //     title: "Thông báo",
                //     message: "Không thể vẽ đường đi trên bản đồ do thiếu dữ liệu tọa độ!",
                // });
                $.alert({
                    title: "Thông báo",
                    content:
                        "Không thể vẽ đường đi trên bản đồ do thiếu dữ liệu tọa độ!",
                });
            }
        }
    );
}
// get info of transport
function getInfoTransport() {
    return (transport = {
        transport: $("#transport").val(),
        from: $("#transport-from").val(),
        to: $("#transport-to").val(),
        "start-date": $("#transport-start-date").val(),
        "start-time": $("#transport-start-time").val().replace(regex, ""),
        "end-date": $("#transport-end-date").val(),
        "end-time": $("#transport-end-time").val().replace(regex, ""),
        "number-transport": $("#number-transport").val(),
        "number-booking": $("#number-booking").val(),
    });
}
// get info of accommodation
function getInfoAccommodation() {
    return (accommodation = {
        name: $("#hotel").val(),
        address: $("#hotel-address").val(),
        "checkin-date": $("#check-in-date").val(),
        "checkin-time": $("#check-in-time").val().replace(regex, ""),
        "checkout-date": $("#check-out-date").val(),
        "checkout-time": $("#check-out-time").val().replace(regex, ""),
        room: $("#room-number").val(),
        "number-booking": $("#number-booking-hotel").val(),
    });
}
// get info of tour
function getTourInfo() {
    try {
        return (tourInfo = {
            "tour-name": $("#tour-name").val(),
            duration: duration,
            "start-date": $("#start-date").val(),
            "end-date": $("#end-date").val(),
            people: $("#adult-number").val(),
        });
    } catch (e) {
        console.log("getTourInfo: " + e.message);
    }
}
// get tour tag
function getTourTag() {
    try {
        return $("#interested").val();
    } catch (e) {
        console.log("getTourTag: " + e.message);
    }
}
// get tour detail
function getTourDetail() {
    try {
        tourDetail = [];
        for (var i = 1; i <= duration; i++) {
            day = [];
            $("#lst_place_date_" + i + " li").each(function () {
                day.push({
                    attr_id: $(this).attr("data-id"),
                    time: $(this)
                        .find(".change-time")
                        .text()
                        .replace(regex, ""),
                });
            });
            tourDetail.push(day);
        }
        return tourDetail;
    } catch (e) {
        console.log("getTourDetail: " + e.message);
    }
}
//
function save() {
    try {
        $.ajax({
            type: "POST",
            url: "/itinerary-custom/save",
            data: {
                transport: getInfoTransport(),
                accommodation: getInfoAccommodation(),
                info: getTourInfo(),
                tag: getTourTag(),
                detail: getTourDetail(),
                mode: mode,
            },
            success: function (res) {
                $.alert({
                    title: "Thông báo!",
                    content: "Lưu thành công",
                    buttons: {
                        OK: function () {
                            location.href = "/personal-itinerary";
                        },
                    },
                });
            },
        });
    } catch (e) {}
}
