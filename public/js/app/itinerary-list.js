$(document).ready(function() {
    try {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        initialize();
        initEvent();
        $("#travel_num_day").val(1);
    } catch (e) {
        alert("document " + e);
    }

});

function initialize() {
    try{
        $('.money').each(function() {
            var money = $(this).text().replace(/\s/g,'');
            $(this).text(money.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        });

    } catch (e) {
        alert("initialize " + e);
    }
}
function initEvent() {
    try{
        $(document).on('click', '.link-action', function() {
            var tour_id = $(this).find( "a" ).attr('tour-id');
            window.location = '/itinerary-detail?from=itinerary-list&tour_id='+ tour_id;
        });

        $(document).on('change', '#travel_num_day', function() {
            getTour();
        });
        $(document).on('change',  "input[name='filter-interests[]']" , function() {
            getTour();
        });
    } catch (e) {
        alert("initEvent " + e);
    }
}
function getTour() {
    var data = getInfo();
    console.log(data);
    $.ajax({
        type       : 'POST',
        url        : '/itinerary-list/search',
        data       : data,
        success    : function (res) {
            $("#section-result").html(res);
        }
    });
}
function getInfo() {
    var day = $('#travel_num_day').val();
    var arr = [];
    $("input[name='filter-interests[]']:checked").each(function() {
        arr.push($(this).attr('name-interested'));
    });
    return {
        duration : day,
        interested : arr.join()
    };
}
