$(document).ready(function () {
    try {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        initialize();
        initEvent();
    } catch (e) {
        window.alert("document " + e);
    }
});
function initialize() {
    try {
        $("#interested").selectpicker();
        $("#duration").selectpicker();
    } catch (e) {
        window.alert("initialize " + e);
    }
}
function initEvent() {
    try {
        // for sticky navigation
        $(".section-work").waypoint(
            function (direction) {
                if (direction === "down") {
                    $("nav").addClass("sticky");
                } else {
                    $("nav").removeClass("sticky");
                }
            },
            {
                offset: "75px",
            }
        );
        $(".btn-plan").click(function () {
            search();
            // alert($('#interested').val());
        });
        $(document).on("click", ".link-action", function () {
            var tour_id = $(this).find("a").attr("tour-id");
            window.location =
                "/itinerary-detail?from=itinerary-list&tour_id=" + tour_id;
        });
    } catch (e) {
        window.alert("initEvent " + e);
    }
}
/**
 *|--------------------------------------------------------------------------
 *| Save data search to session
 *|--------------------------------------------------------------------------
 *| Package       : Master
 *| @author       : trinhdtk - INS211 - trinhdtk@ans-asia.com
 *| @created date : 2018/10/09
 *| Description   :
 */
function search() {
    if (validate()) {
        $.ajax({
            type: "POST",
            url: "/save-info",
            data: {
                duration: $("#duration").val(),
                interested: $("#interested").val(),
            },
            success: function (data) {
                if (data["status"]) {
                    window.location = "/itinerary-list?from=homepage";
                }
            },
        });
    } else {
        $.alert({
            title: "Thông báo!",
            content: "Hãy nhập thông tin tìm kiếm",
            buttons: {
                OK: {
                    btnClass: "btn-red any-other-class", // multiple classes.
                },
            },
        });
    }
}
/**
 *|--------------------------------------------------------------------------
 *| Validate form
 *|--------------------------------------------------------------------------
 *| Package       : Master
 *| @author       : trinhdtk - INS211 - trinhdtk@ans-asia.com
 *| @created date : 2018/10/09
 *| Description   :
 */
function validate() {
    if ($("#interested").val() === null || $("#duration").val() === "") {
        return false;
    }
    return true;
}
