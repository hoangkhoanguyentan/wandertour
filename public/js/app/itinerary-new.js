var transport = {};
var accommodation = {};
var tourInfo = {};
var tourTag;
var tourDetail = [];
var regex = /:/gi;
var isSaving = false;

var tranOpenTime = "Mở cửa";
var enTranOpenTime = "Open";

var tranCloseTime = "Đóng cửa";
var enTranCloseTime = "Close";

$(document).ready(function () {
    try {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        initialize();
        initEvent();
        $("#tour-name").focus();

        var tourName = document.getElementById("tour-name");
    } catch (e) {
        window.alert("document " + e);
    }
});

// initialize object
function initialize() {
    try {
        $("#interested").select2();
        initDateTimePicker();
        var ps = new PerfectScrollbar(".lst-item-form");
        // var ps = new PerfectScrollbar('');
        var html = Mustache.render($("#item_tour_detail").html(), {
            Geo: 1,
            DataNation: 1,
            Day: 1,
            PlaceInfo: 1,
            Image: 1,
            Time: 11,
            PlaceName: 11,
            TimeOpen1: 22,
            TimeOpen: 33,
            TimeClose: 44,
            tranOpenTime: lang === "vi" ? tranOpenTime : enTranOpenTime,
            tranCloseTime: lang === "vi" ? tranCloseTime : enTranCloseTime,
            VisitTime: 55,
        });
        // console.log(html);
        $(".list-attraction-bar").waypoint(
            function (direction) {
                if (direction === "down") {
                    $(".list-attraction-bar").addClass(
                        "list-attraction-bar-sticky"
                    );
                } else {
                    $(".list-attraction-bar").removeClass(
                        "list-attraction-bar-sticky"
                    );
                }
            },
            {
                offset: "50px",
            }
        );
        $(document).on("click", "#save-tour", function () {
            save();
        });
    } catch (e) {
        window.alert("initialize " + e);
    }
}

function updateTimeEvent() {
    $(".time").each(function () {
        var time = $(this).text().replace(/\s/g, ""); // Loại bỏ khoảng trắng
        // Kiểm tra xem chuỗi đã chứa dấu ":" hay chưa
        if (time.indexOf(":") === -1) {
            // Nếu chưa có dấu ":", thêm dấu ":" vào sau 2 ký tự đầu tiên
            time = [time.slice(0, 2), ":", time.slice(2)].join("");
        } else {
            // Nếu đã có dấu ":", loại bỏ tất cả các dấu ":" ngoại trừ dấu ":" đầu tiên
            time = time.replace(/:/g, (match, offset, string) => {
                return offset === string.indexOf(":") ? ":" : "";
            });
        }
        $(this).text(time);
    });
}
// initialize object event
function initEvent() {
    try {
        $(".time").each(function () {
            var time = $(this).text().replace(/\s/g, "");
            $(this).text([time.slice(0, 2), ":", time.slice(2)].join(""));
        });
        $("#btn-transport").click(function () {
            $("#transport-popup").modal("show");
        });
        $("#btn-accommodation").click(function () {
            $("#accommodation-popup").modal("show");
        });
        $("#btn-transport-save").click(function () {
            getInfoTransport();
            // console.log(transport);
            $("#transport-popup").modal("hide");
        });
        $("#btn-accommodation-save").click(function () {
            getInfoAccommodation();
            $("#accommodation-popup").modal("hide");
        });
        $(document).on("keyup", ".input-findname", function () {
            searchAttraction();
        });
        //init drop list item place search right
        Sortable.create(document.getElementById("tab-panel-1"), {
            sort: false,
            group: {
                name: "place_item",
                pull: "clone",
                put: false,
            },
            animation: 150,
            scroll: true,
        });
        $(document).on("change", "#hotel", function () {
            $("#hotel-name").val($(this).val());
        });
        $(document).on("change", "#hotel-name", function () {
            $("#hotel").val($(this).val());
        });

        setTimeout(initDatePlaceSortDrop(), 200);

        //Xoa dia diem trong ngay
        $(document).on("click", ".remove-place", function () {
            var $this = $(this);
            $.confirm({
                title: "Xác nhận!",
                content: "Bạn muốn xóa địa điểm này khỏi lịch trình?",
                buttons: {
                    "Xác nhận": {
                        btnClass: "btn-blue",
                        action: function () {
                            var dayId = $this
                                .closest(".lst-date-item")
                                .attr("date-no");
                            $this.closest("li").remove();
                            var checkLength = $(
                                "#lst_place_date_" + dayId
                            ).find("li").length;
                            if (checkLength == 0) {
                                $("#edit-tour-" + dayId)
                                    .find(".place-tour-day")
                                    .trigger("click");
                            }
                            var lstDayOfTourOnMapOnAdd = [];
                            $("#lst_place_date_" + dayId)
                                .find("li.li-parent")
                                .each(function () {
                                    lstDayOfTourOnMapOnAdd.push({
                                        location: $(this).attr("data-geo"),
                                    });
                                });
                        },
                    },
                    Hủy: function () {},
                },
            });
        });
    } catch (e) {
        window.alert("initEvent " + e);
    }
}
// int date time picker
function initDateTimePicker() {
    var today = new Date();
    var nextDay = new Date(today.getDate() + 1);
    $(".end-date-class").datetimepicker({
        format: "YYYY/MM/DD",
    });
    $(".start-date-class").datetimepicker({
        format: "YYYY/MM/DD",
        minDate: today,
    });
    $(".transport-start-date").datetimepicker({
        format: "YYYY/MM/DD",
        minDate: today,
    });
    $(".transport-end-date").datetimepicker({
        format: "YYYY/MM/DD",
        minDate: today,
    });
    $("#transport-start-time").datetimepicker({
        format: "HH:mm",
    });
    $("#transport-end-time").datetimepicker({
        format: "HH:mm",
    });
    $(".check-in-date").datetimepicker({
        format: "YYYY/MM/DD",
        minDate: today,
    });
    $(".check-out-date").datetimepicker({
        format: "YYYY/MM/DD",
        minDate: today,
    });
    $("#check-in-time").datetimepicker({
        format: "HH:mm",
    });
    $("#check-out-time").datetimepicker({
        format: "HH:mm",
    });
}
// search attraction
function searchAttraction() {
    try {
        $.ajax({
            type: "POST",
            url: "/itinerary-custom/search",
            data: {
                attraction_nm: $("input[name='search-attraction']").val(),
            },
            success: function (data) {
                $("#tab-panel-1").html(data);
            },
        });
    } catch (e) {
        console.log(e.message);
    }
}
function findAttractionByTag(tag) {
    try {
        $.ajax({
            type: "POST",
            url: "/itinerary-custom/searchByTag",
            data: {
                attraction_tag: tag,
            },
            success: function (data) {
                $("#tab-panel-1").html(data);
            },
        });
    } catch (e) {
        console.log(e.message);
    }
}
// init drop list date
function initDatePlaceSortDrop() {
    for (var i = 1; i <= duration; i++) {
        Sortable.create(document.getElementById("lst_place_date_" + i), {
            sort: true,
            group: {
                name: "place_item",
                pull: false,
                put: true,
            },
            scroll: true,
            animation: 250,
            onAdd: function (evt) {
                var newIDItem =
                    evt.item.attributes.getNamedItem("data-id").value;
                var imgSrc = evt.item.childNodes[1].children[0].src;
                var placeName = evt.item.children[1].children[0].textContent;
                var areaDay = evt.target.attributes
                    .getNamedItem("id")
                    .value.split("_")[3];
                // var data_tour = evt.item.attributes.getNamedItem("data-tour").value;
                var data_geo =
                    evt.item.attributes.getNamedItem("data-geo").value;
                var timeopen =
                    evt.item.attributes.getNamedItem("data-time-open").value;
                var timeclose =
                    evt.item.attributes.getNamedItem("data-time-close").value;
                var visittime =
                    evt.item.attributes.getNamedItem("data-visit-time").value;
                var arr = {
                    Geo: data_geo,
                    Day: areaDay,
                    PlaceInfo: newIDItem,
                    Image: imgSrc,
                    PlaceName: placeName,
                    TimeOpen: timeopen,
                    TimeClose: timeclose,
                    VisitTime: visittime,
                };

                var lstDayOfTourOnMapOnAdd = [];
                var arrayCheck = [];
                $("#lst_place_date_" + areaDay)
                    .find("li.li-parent")
                    .each(function () {
                        var itemAttr = $(this).attr("data-id");
                        if (itemAttr === newIDItem) {
                            arrayCheck.push(newIDItem);
                        }
                    });
                if (
                    evt.from.id === "tab-panel-1" ||
                    evt.from.id === "list_place_res" ||
                    evt.from.id === "list_place_hotel" ||
                    evt.from.id === "list_place_shop"
                ) {
                    if (arrayCheck.length > 0) {
                        var itemAdd = $(evt.item);
                        itemAdd.remove();
                        // bootbox.alert({
                        //     title: "Thông báo",
                        //     message: "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!",
                        // });
                        $.alert({
                            title: lang == "vi" ? "Thông báo" : "Notification",
                            content:
                                lang == "vi"
                                    ? "Không thể chọn hai địa điểm giống nhau trong một ngày của lịch trình!"
                                    : "Cannot choose two same places in one day of the itinerary!",
                        });
                    } else {
                        var timeopen1;
                        if (timeopen === "00:00") {
                            timeopen1 = "07:30";
                        } else {
                            timeopen1 = timeopen;
                        }
                        var html = Mustache.render(
                            $("#item_tour_detail").html(),
                            {
                                Geo: data_geo,
                                // DataNation: data_tour,
                                Day: areaDay,
                                PlaceInfo: newIDItem,
                                Image: imgSrc,
                                Time: timeopen1,
                                PlaceName: placeName,
                                TimeOpen1: timeopen1,
                                TimeOpen: timeopen1,
                                TimeClose: timeclose,
                                tranOpenTime:
                                    lang === "vi"
                                        ? tranOpenTime
                                        : enTranOpenTime,
                                tranCloseTime:
                                    lang === "vi"
                                        ? tranCloseTime
                                        : enTranCloseTime,
                                VisitTime: visittime,
                            }
                        );
                        evt.item.outerHTML = html;
                        $("#lst_place_date_" + areaDay)
                            .find("li.li-parent")
                            .each(function () {
                                lstDayOfTourOnMapOnAdd.push({
                                    location: $(this).attr("data-geo"),
                                });
                            });
                    }
                }
                updateTimeEvent();
            },
            onSort: function (evt) {
                var lstDayOfTourOnMapOnAdd = [];
                var areaDay = evt.to.attributes[1].value.split("_")[3];
                $("#lst_place_date_" + areaDay)
                    .find("li.li-parent")
                    .each(function () {
                        lstDayOfTourOnMapOnAdd.push({
                            location: $(this).attr("data-geo"),
                        });
                    });
                $("#lst_place_date_" + areaDay + " li.li-parent:last-child")
                    .children(".calculater-route-info")
                    .html("");
            },
        });
    }
}
// get info of transport
function getInfoTransport() {
    return (transport = {
        transport: $("#transport").val(),
        from: $("#transport-from").val(),
        to: $("#transport-to").val(),
        "start-date": $("#transport-start-date").val(),
        "start-time": $("#transport-start-time").val().replace(regex, ""),
        "end-date": $("#transport-end-date").val(),
        "end-time": $("#transport-end-time").val().replace(regex, ""),
        "number-transport": $("#number-transport").val(),
        "number-booking": $("#number-booking").val(),
    });
}
// get info of accommodation
function getInfoAccommodation() {
    return (accommodation = {
        name: $("#hotel").val(),
        address: $("#hotel-address").val(),
        "checkin-date": $("#check-in-date").val(),
        "checkin-time": $("#check-in-time").val().replace(regex, ""),
        "checkout-date": $("#check-out-date").val(),
        "checkout-time": $("#check-out-time").val().replace(regex, ""),
        room: $("#room-number").val(),
        "number-booking": $("#number-booking-hotel").val(),
    });
}
// get info of tour
function getTourInfo() {
    try {
        // Get the start and end date values from the input fields
        const startDate = new Date($("#start-date").val());
        const endDate = new Date($("#end-date").val());

        // Calculate the time difference in milliseconds
        const timeDifference = endDate - startDate;

        // Calculate the number of days
        const newDuration = timeDifference / (1000 * 3600 * 24);

        return (tourInfo = {
            "tour-name": $("#tour-name").val(),
            duration: newDuration,
            "start-date": $("#start-date").val(),
            "end-date": $("#end-date").val(),
            people: $("#adult-number").val(),
        });
    } catch (e) {
        console.log("getTourInfo: " + e.message);
    }
}
// get tour tag
function getTourTag() {
    try {
        return $("#interested").val();
    } catch (e) {
        console.log("getTourTag: " + e.message);
    }
}
// get tour detail
function getTourDetail() {
    try {
        tourDetail = [];
        for (var i = 1; i <= duration; i++) {
            day = [];
            $("#lst_place_date_" + i + " li").each(function () {
                day.push({
                    attr_id: $(this).attr("data-id"),
                    time: $(this)
                        .find(".change-time")
                        .text()
                        .replace(regex, ""),
                    opening_time_start: $(this)
                        .find("#opening_time_start")
                        .text()
                        .replace(regex, ""),
                    opening_time_end: $(this)
                        .find("#opening_time_end")
                        .text()
                        .replace(regex, ""),
                    day: i,
                });
            });
            tourDetail.push(day);
        }
        var flattenedDetails = [].concat(...tourDetail);
        return flattenedDetails;
    } catch (e) {
        console.log("getTourDetail: " + e.message);
    }
}
//
function save() {
    try {
        if (!isSaving) {
            isSaving = true;
            $.ajax({
                type: "POST",
                url: "/update-tour",
                data: {
                    transport: getInfoTransport(),
                    accommodation: getInfoAccommodation(),
                    info: getTourInfo(),
                    tag: getTourTag(),
                    detail: getTourDetail(),
                    mode: mode,
                },
                success: function (res) {
                    $.alert({
                        title: "Thông báo!",
                        content: "Lưu thành công",
                        buttons: {
                            OK: function () {
                                location.href = "/personal-itinerary?menu=tour";
                            },
                        },
                    });
                },
            });
        }
    } catch (e) {}
}

function startDateChange(e) {
    var selectedDate = new Date(e.value);
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    if (selectedDate < currentDate) {
        alert("Selected date cannot be earlier than the current date.");
        return false;
    }
}

function endDateChange(e) {
    var selectedDate = new Date(e.value);
    var startDate = new Date(document.getElementById("start-date").value);

    if (selectedDate < startDate) {
        alert("Selected date cannot be earlier than the current date.");
        return false;
    }

    // Đổi định dạng ngày về milliseconds và tính sự chênh lệch
    var timeDiff = selectedDate.getTime() - startDate.getTime();

    // Chuyển đổi milliseconds thành số ngày
    var dayDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

    if (dayDiff === 0) {
        // selectedDate và startDate là cùng một ngày, không thực hiện gì cả
        alert("Start date and end date cannot be the same.");
        return false;
    }
    if (dayDiff === duration) {
        return false;
    }

    if (dayDiff > 45) {
        alert("The maximum duration is 45 days.");
        return false;
    }
    loadTourDetail();
}

function loadTourDetail() {
    $.ajax({
        type: "POST",
        url: "/load-tour-detail",
        data: {
            tour_info: getTourInfo(),
            tour_detail: getTourDetail(),
        },
        success: function (res) {
            $("#tour-detail").html(res);
        },
    });
}
