$(document).ready(function() {
    try {
        initialize();
        initEvent();
    } catch (e) {
        alert("document " + e);
    }

});

function initialize() {
    try{
        $('.time').each(function() {
            var time = $(this).text().replace(/\s/g,'');
            $(this).text([time.slice(0, 2), ":", time.slice(2)].join(''));
        });
        $('body').scrollspy({target: ".navbar", offset: 20});
        // for sticky navigation
        $('#tour-support').waypoint(function(direction){
            if(direction === "down"){
                $('#tour-support').addClass('tour-support-sticky');
                $('#sidemenu-tour').addClass('sidemenu-tour-sticky');
                // console.log('down');
            } else {
                $('#tour-support').removeClass('tour-support-sticky');
                $('#sidemenu-tour').removeClass('sidemenu-tour-sticky');
                // console.log('up');
            }
        }, {
            offset: '80px'
        });


    } catch (e) {
        alert("initialize " + e);
    }
}
function initEvent() {
    try{
        $("#sidemenu-tour a").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();
                // Store hash
                var hash = this.hash;
                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){
                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            }  // End if
        });
        $(document).on('click', '.btn-collapse', function() {
            if ($(this).prop('name') === 'arrow-dropleft-circle'){
                $(this).prop('name','arrow-dropdown-circle');
            } else {
                $(this).prop('name','arrow-dropleft-circle');
            }
        });
        $(document).on('click', '#custom-tour', function(){
            var url = new URL(window.location.href);
            var tour_id = url.searchParams.get("tour_id");
            location.href = "/itinerary-custom?tour_id=" + tour_id;
        });

    } catch (e) {
        alert("initEvent " + e);
    }
}
