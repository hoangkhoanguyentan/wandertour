<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttractionImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['attr_id' => 1, 'img_id' => 8, 'number' => 1],
            ['attr_id' => 2, 'img_id' => 11, 'number' => 1],
            ['attr_id' => 2, 'img_id' => 12, 'number' => 2],
            ['attr_id' => 3, 'img_id' => 13, 'number' => 1],
            ['attr_id' => 3, 'img_id' => 14, 'number' => 2],
            ['attr_id' => 4, 'img_id' => 15, 'number' => 1],
            ['attr_id' => 4, 'img_id' => 16, 'number' => 2],
            ['attr_id' => 5, 'img_id' => 17, 'number' => 1],
            ['attr_id' => 5, 'img_id' => 18, 'number' => 2],
            ['attr_id' => 6, 'img_id' => 19, 'number' => 1],
            ['attr_id' => 6, 'img_id' => 20, 'number' => 2],
            ['attr_id' => 7, 'img_id' => 21, 'number' => 1],
            ['attr_id' => 7, 'img_id' => 22, 'number' => 2],
            ['attr_id' => 8, 'img_id' => 23, 'number' => 1],
            ['attr_id' => 8, 'img_id' => 24, 'number' => 2],
            ['attr_id' => 9, 'img_id' => 1, 'number' => 1],
            ['attr_id' => 9, 'img_id' => 25, 'number' => 2],
            ['attr_id' => 10, 'img_id' => 26, 'number' => 1],
            ['attr_id' => 10, 'img_id' => 27, 'number' => 2],
            ['attr_id' => 11, 'img_id' => 28, 'number' => 1],
            ['attr_id' => 11, 'img_id' => 29, 'number' => 2],
            ['attr_id' => 12, 'img_id' => 30, 'number' => 1],
            ['attr_id' => 12, 'img_id' => 31, 'number' => 2],
            ['attr_id' => 13, 'img_id' => 32, 'number' => 1],
            ['attr_id' => 13, 'img_id' => 33, 'number' => 2],
            ['attr_id' => 14, 'img_id' => 3, 'number' => 1],
            ['attr_id' => 14, 'img_id' => 34, 'number' => 2],
            ['attr_id' => 15, 'img_id' => 6, 'number' => 1],
            ['attr_id' => 15, 'img_id' => 35, 'number' => 2],
            ['attr_id' => 16, 'img_id' => 36, 'number' => 1],
            ['attr_id' => 16, 'img_id' => 37, 'number' => 2],
            ['attr_id' => 17, 'img_id' => 38, 'number' => 1],
            ['attr_id' => 17, 'img_id' => 39, 'number' => 2],
            ['attr_id' => 18, 'img_id' => 40, 'number' => 1],
            ['attr_id' => 18, 'img_id' => 41, 'number' => 2],
            ['attr_id' => 19, 'img_id' => 42, 'number' => 1],
            ['attr_id' => 19, 'img_id' => 43, 'number' => 2],
            ['attr_id' => 20, 'img_id' => 5, 'number' => 1],
            ['attr_id' => 20, 'img_id' => 44, 'number' => 2],
            ['attr_id' => 21, 'img_id' => 45, 'number' => 1],
            ['attr_id' => 21, 'img_id' => 46, 'number' => 2],
            ['attr_id' => 22, 'img_id' => 47, 'number' => 1],
            ['attr_id' => 22, 'img_id' => 48, 'number' => 2],
            ['attr_id' => 23, 'img_id' => 49, 'number' => 1],
            ['attr_id' => 23, 'img_id' => 50, 'number' => 2],
            ['attr_id' => 24, 'img_id' => 7, 'number' => 1],
            ['attr_id' => 24, 'img_id' => 51, 'number' => 2],
            ['attr_id' => 25, 'img_id' => 52, 'number' => 1],
            ['attr_id' => 26, 'img_id' => 1, 'number' => 1],
            ['attr_id' => 26, 'img_id' => 53, 'number' => 2],
            ['attr_id' => 27, 'img_id' => 54, 'number' => 1],
            ['attr_id' => 27, 'img_id' => 55, 'number' => 2],
            ['attr_id' => 28, 'img_id' => 56, 'number' => 1],
            ['attr_id' => 28, 'img_id' => 57, 'number' => 2],
            ['attr_id' => 29, 'img_id' => 58, 'number' => 1],
            ['attr_id' => 29, 'img_id' => 59, 'number' => 2],
        ];

        DB::table('attraction_image')->insert($data);
    }
}
