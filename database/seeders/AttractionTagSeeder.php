<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttractionTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['attr_id' => 1, 'img_id' => 8, 'tag_id' => 11],
            ['attr_id' => 2, 'img_id' => 11, 'tag_id' => 5],
            ['attr_id' => 3, 'img_id' => 13, 'tag_id' => 12],
            ['attr_id' => 4, 'img_id' => 15, 'tag_id' => 4],
            ['attr_id' => 5, 'img_id' => 17, 'tag_id' => 9],
            ['attr_id' => 6, 'img_id' => 19, 'tag_id' => 6],
            ['attr_id' => 7, 'img_id' => 21, 'tag_id' => 5],
            ['attr_id' => 8, 'img_id' => 23, 'tag_id' => 6],
            ['attr_id' => 9, 'img_id' => 1, 'tag_id' => 1],
            ['attr_id' => 10, 'img_id' => 26, 'tag_id' => 8],
            ['attr_id' => 11, 'img_id' => 28, 'tag_id' => 5],
            ['attr_id' => 12, 'img_id' => 30, 'tag_id' => 13],
            ['attr_id' => 13, 'img_id' => 32, 'tag_id' => 1],
            ['attr_id' => 14, 'img_id' => 3, 'tag_id' => 7],
            ['attr_id' => 15, 'img_id' => 6, 'tag_id' => 1],
            ['attr_id' => 16, 'img_id' => 36, 'tag_id' => 5],
            ['attr_id' => 17, 'img_id' => 38, 'tag_id' => 10],
            ['attr_id' => 18, 'img_id' => 40, 'tag_id' => 5],
            ['attr_id' => 19, 'img_id' => 42, 'tag_id' => 13],
            ['attr_id' => 20, 'img_id' => 5, 'tag_id' => 11],
            ['attr_id' => 21, 'img_id' => 45, 'tag_id' => 12],
            ['attr_id' => 22, 'img_id' => 47, 'tag_id' => 13],
            ['attr_id' => 23, 'img_id' => 49, 'tag_id' => 2],
            ['attr_id' => 24, 'img_id' => 7, 'tag_id' => 9],
            ['attr_id' => 25, 'img_id' => 52, 'tag_id' => 8],
            ['attr_id' => 26, 'img_id' => 1, 'tag_id' => 5],
            ['attr_id' => 27, 'img_id' => 54, 'tag_id' => 12],
            ['attr_id' => 28, 'img_id' => 56, 'tag_id' => 2],
            ['attr_id' => 29, 'img_id' => 58, 'tag_id' => 7],
            // Thêm dòng dữ liệu khác nếu cần
        ];

        DB::table('attraction_tag')->insert($data);
    }
}
