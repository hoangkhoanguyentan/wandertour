<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TourDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        for ($i = 1; $i <= 1; $i++) {
            DB::table('tour_detail')->insert([
                'tour_id' => rand(1, 2), // Thay thế bằng id tour tương ứng
                'attr_id' => rand(1, 10), // Thay thế bằng id attraction tương ứng
                'number' => rand(1, 5),
                'day' => rand(1, 5),
                'time' => '9:00 AM', // Thay thế bằng thời gian tương ứng
            ]);
        }
    }
}
