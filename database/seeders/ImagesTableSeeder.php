<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['img_id' => 1, 'link' => 'ba_na.jpg'],
            ['img_id' => 2, 'link' => 'cau_rong_1.jpg'],
            ['img_id' => 3, 'link' => 'asia_park.jpg'],
            ['img_id' => 4, 'link' => 'cau_song_han.jpg'],
            ['img_id' => 5, 'link' => 'chua_linh_ung.jpg'],
            ['img_id' => 6, 'link' => 'deo_hai_van.jpg'],
            ['img_id' => 7, 'link' => 'dhc_marina.jpeg'],
            ['img_id' => 8, 'link' => 'ngu_hanh_son.jpg'],
            ['img_id' => 9, 'link' => 'son_tra'],
            ['img_id' => 10, 'link' => 'tran_thi_ly.jpg'],
            ['img_id' => 11, 'link' => 'an_trua.jpg'],
            ['img_id' => 12, 'link' => 'an_trua_1.jpeg'],
            ['img_id' => 13, 'link' => 'bao_tang_cham.jpg'],
            ['img_id' => 14, 'link' => 'bao_tang_cham_1.jpg'],
            ['img_id' => 15, 'link' => 'bien_pvd.jpg'],
            ['img_id' => 16, 'link' => 'bien_pvd_1.jpg'],
            ['img_id' => 17, 'link' => 'du_thuyen.jpeg'],
            ['img_id' => 18, 'link' => 'du_thuyen_1.jpg'],
            ['img_id' => 19, 'link' => 'sky_36.jpg'],
            ['img_id' => 20, 'link' => 'sky_36_1.jpg'],
            ['img_id' => 21, 'link' => 'hai_san.jpg'],
            ['img_id' => 22, 'link' => 'hai_san_1.jpg'],
            ['img_id' => 23, 'link' => 'alacarte.png'],
            ['img_id' => 24, 'link' => 'alacarte.jpg'],
            ['img_id' => 25, 'link' => 'ba_na_1.jpg'],
            ['img_id' => 26, 'link' => 'suoi_than_tai.jpg'],
            ['img_id' => 27, 'link' => 'suoi_than_tai_1.jpg'],
            ['img_id' => 28, 'link' => 'deli.jpg'],
            ['img_id' => 29, 'link' => 'deli_1.jpg'],
            ['img_id' => 30, 'link' => '3D.jpg'],
            ['img_id' => 31, 'link' => '3D_1.jpg'],
            ['img_id' => 32, 'link' => 'tien_son.jpg'],
            ['img_id' => 33, 'link' => 'tien_son_1.jpg'],
            ['img_id' => 34, 'link' => 'asia_park_1.jpg'],
            ['img_id' => 35, 'link' => 'deo_hai_van_1.jpg'],
            ['img_id' => 36, 'link' => 'nam_o.jpg'],
            ['img_id' => 37, 'link' => 'nam_o_1.jpg'],
            ['img_id' => 38, 'link' => 'suoi_luong.jpg'],
            ['img_id' => 39, 'link' => 'suoi_luong_1.jpg'],
            ['img_id' => 40, 'link' => 'ba_van.jpg'],
            ['img_id' => 41, 'link' => 'ba_van_1.jpg'],
            ['img_id' => 42, 'link' => 'ban_co.jpg'],
            ['img_id' => 43, 'link' => 'ban_co.png'],
            ['img_id' => 44, 'link' => 'chua_linh_ung_1.jpg'],
            ['img_id' => 45, 'link' => 'souvenir.jpg'],
            ['img_id' => 46, 'link' => 'souvenir_1.jpg'],
            ['img_id' => 47, 'link' => 'hoi_an.jpg'],
            ['img_id' => 48, 'link' => 'hoi_an_1.jpg'],
            ['img_id' => 49, 'link' => 'my_son.jpg'],
            ['img_id' => 50, 'link' => 'my_son_1.jpg'],
            ['img_id' => 51, 'link' => 'dhc_marina_1.jpg'],
            ['img_id' => 52, 'link' => 'hpt.jpg'],
            ['img_id' => 53, 'link' => 'hpt_1.jpg'],
            ['img_id' => 54, 'link' => 'cho_han.jpg'],
            ['img_id' => 55, 'link' => 'cho_han_1.png'],
            ['img_id' => 56, 'link' => 'dong_dinh.jpg'],
            ['img_id' => 57, 'link' => 'dong_dinh_1.jpg'],
            ['img_id' => 58, 'link' => 'cong_vien_bo_cau.jpg'],
            ['img_id' => 59, 'link' => 'cong_vien_bo_cau_1.jpg'],
        ];

        DB::table('image')->insert($data);
    }
}
