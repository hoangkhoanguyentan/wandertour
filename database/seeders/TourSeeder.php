<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            DB::table('tour')->insert([
                'tour_name' => 'Tour ' . $i,
                'tour_duration' => rand(5, 15), // Thay thế bằng dữ liệu mẫu phù hợp
                'img_id' => rand(1, 60), // Thay thế bằng id của ảnh tương ứng
                'tour_budget' => rand(500, 5000),
                'start_date' => now(),
                'end_date' => now()->addDays(rand(1, 30)),
                'people' => rand(5, 20),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
