<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tag')->insert([
            ['name' => 'Phong cảnh', 'tag_id' => 1],
            ['name' => 'Lịch sử', 'tag_id' => 2],
            ['name' => 'Cầu', 'tag_id' => 3],
            ['name' => 'Biển', 'tag_id' => 4],
            ['name' => 'Ẩm thực', 'tag_id' => 5],
            ['name' => 'Cafe', 'tag_id' => 6],
            ['name' => 'Công viên giải trí', 'tag_id' => 7],
            ['name' => 'Công viên nước', 'tag_id' => 8],
            ['name' => 'Chụp ảnh', 'tag_id' => 9],
            ['name' => 'Suối', 'tag_id' => 10],
            ['name' => 'Chùa', 'tag_id' => 11],
            ['name' => 'Lưu niệm', 'tag_id' => 12],
            ['name' => 'Di sản', 'tag_id' => 13],
            ['name' => 'Mạo hiểm', 'tag_id' => 14],
            ['name' => 'Làng nghề', 'tag_id' => 15],
        ]);
    }
}
