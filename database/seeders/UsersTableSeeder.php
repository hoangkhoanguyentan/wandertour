<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['user_id' => 1, 'username' => '1', 'password' => '1'],
            ['user_id' => 2, 'username' => '2', 'password' => '1'],
            ['user_id' => 999, 'username' => 'admin', 'password' => 'admin'],

        ];

        DB::table('user')->insert($data);
    }
}
