<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class AttractionImageFactory extends Factory
{
    protected $model = AttractionImage::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'attr_id' => $this->faker->numberBetween(1, 30),
            'img_id' => $this->faker->numberBetween(1, 59),
            'number' => $this->faker->numberBetween(1, 2),
        ];
    }
}
