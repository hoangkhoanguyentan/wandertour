<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attraction>
 */
class AttractionFactory extends Factory
{
    protected $model = Attraction::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'attr_id' => $this->faker->unique()->numberBetween(1, 30),
            'attr_name' => $this->faker->sentence(2),
            'attr_address' => $this->faker->address,
            'lat' => $this->faker->latitude,
            'long' => $this->faker->longitude,
            'opening_time_start' => $this->faker->time('H:i'),
            'opening_time_end' => $this->faker->time('H:i'),
            'attr_duration' => $this->faker->time('H:i'),
            'best_time_start' => $this->faker->time('H:i'),
            'best_time_end' => $this->faker->time('H:i'),
            'price' => $this->faker->numberBetween(0, 100),
            'detail' => $this->faker->paragraph,
        ];
    }
}
