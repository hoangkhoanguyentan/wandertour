<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('tour_tag')) {
            Schema::create('tour_tag', function (Blueprint $table) {
                $table->integer('tour_id')->unsigned();
                $table->integer('tag_id')->unsigned();
                $table->foreign('tour_id')->references('tour_id')->on('tour')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('tag_id')->references('tag_id')->on('tag')->onDelete('cascade')->onUpdate('cascade');
                $table->primary(['tour_id', 'tag_id']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tour_tag');
    }
};
