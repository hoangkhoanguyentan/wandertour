<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('user')) {
            Schema::create('user', function (Blueprint $table) {
                $table->increments('user_id');
                $table->string('username');
                $table->string('password');

                $table->string('email');
                $table->string('phone_number');
                $table->string('full_name');
                $table->string('sex');
                $table->string('year_of_birth');
                $table->string('occupation');

            });
        }
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('accommodation', function (Blueprint $table) {
            $table->dropForeign('accommodation_user_id_foreign');
        });
        Schema::dropIfExists('user');
    }
};
