<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('tour_detail')) {
            Schema::create('tour_detail', function (Blueprint $table) {
                $table->integer('tour_id')->unsigned();
                $table->foreign('tour_id')->references('tour_id')->on('tour')->onDelete('cascade')->onUpdate('cascade');
                $table->integer('attr_id')->unsigned();
                $table->foreign('attr_id')->references('attr_id')->on('attraction')->onDelete('cascade')->onUpdate('cascade');
                $table->integer('number');
                $table->integer('day');
                $table->string('time');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tour_detail');
    }
};
