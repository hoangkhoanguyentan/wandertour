<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('tour_user')) {
            Schema::create('tour_user', function (Blueprint $table) {
                $table->integer('tour_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->foreign('tour_id')->references('tour_id')->on('tour')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
                $table->primary(['tour_id', 'user_id']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tour_user');
    }
};
