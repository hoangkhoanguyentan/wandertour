<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('blog')) {
            Schema::create('blog', function (Blueprint $table) {
                $table->id();
                $table->string('name')->nullable(); // Tên món ăn               
                $table->string('en_name')->nullable(); // Tên món ăn
                $table->string('title')->nullable();
                $table->string('en_title')->nullable();
                $table->string('slug');
                $table->text('content')->nullable();
                $table->text('en_content')->nullable();
                $table->boolean('display')->default(0);
                $table->integer('attr_id')->unsigned();
                $table->integer('img_id')->unsigned()->nullable();
                $table->boolean('type')->default(0); // Sử dụng trường này để xác định loại blog (attraction hoặc gastronomy)
                $table->timestamps();

                $table->foreign('attr_id')->references('attr_id')->on('attraction')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('img_id')->references('img_id')->on('image')->onDelete('cascade')->onUpdate('cascade');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('blog');
    }
};
