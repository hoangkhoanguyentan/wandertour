<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('user_tag')) {
            Schema::create('user_tag', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('tag_id')->unsigned();
                $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('tag_id')->references('tag_id')->on('tag')->onDelete('cascade')->onUpdate('cascade');
                $table->primary(['user_id', 'tag_id']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_tag');
    }
};
