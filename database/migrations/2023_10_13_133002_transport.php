<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('transport')) {
            Schema::create('transport', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('tour_id')->unsigned();
                $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('tour_id')->references('tour_id')->on('tour')->onDelete('cascade')->onUpdate('cascade');
                $table->primary(['user_id', 'tour_id']);
                $table->string('departure')->nullable();
                $table->string('arrival')->nullable();
                $table->date('departure_date')->nullable();
                $table->string('departure_time')->nullable();
                $table->string('arrival_date')->nullable();
                $table->string('arrival_time')->nullable();
                $table->string('number')->nullable();
                $table->string('booking_no')->nullable();
                $table->string('transport')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transport');
    }
};
