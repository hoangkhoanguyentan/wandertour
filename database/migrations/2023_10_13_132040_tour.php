<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('tour')) {
            Schema::create('tour', function (Blueprint $table) {
                $table->increments('tour_id');
                $table->string('tour_name')->nullable();
                $table->string('en_tour_name')->nullable();
                $table->integer('tour_duration');
                $table->integer('img_id')->unsigned();
                $table->foreign('img_id')
                    ->references('img_id')
                    ->on('image')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                $table->decimal('tour_budget');
                $table->date('start_date');
                $table->date('end_date');
                $table->integer('people');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('accommodation', function (Blueprint $table) {
            $table->dropForeign('accommodation_tour_id_foreign');
        });
        Schema::dropIfExists('tour');
    }
};
