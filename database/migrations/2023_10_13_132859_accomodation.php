<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('accommodation')) {
            Schema::create('accomomdation', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('tour_id')->unsigned();
                $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('tour_id')->references('tour_id')->on('tour')->onDelete('cascade')->onUpdate('cascade');
                $table->primary(['user_id', 'tour_id']);
                $table->string('hotel_name');
                $table->string('address');
                $table->date('checkin_date');
                $table->string('checkin_time');
                $table->date('checkout_date');
                $table->string('checkout_time');
                $table->string('booking_no');
                $table->string('room');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('accommodation', function (Blueprint $table) {
            $table->dropForeign('accommodation_user_id_foreign');
            $table->dropForeign('accommodation_tour_id_foreign');
        });
        Schema::dropIfExists('accommodation');
    }
};
