<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('attraction_tag')) {
            Schema::create('attraction_tag', function (Blueprint $table) {
                $table->integer('img_id')->unsigned();
                $table->integer('attr_id')->unsigned();
                $table->integer('tag_id')->unsigned();
                $table->foreign('attr_id')->references('attr_id')->on('attraction')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('tag_id')->references('tag_id')->on('tag')->onDelete('cascade')->onUpdate('cascade');
                $table->integer('number')->nullable()->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attraction_tag');
    }
};
