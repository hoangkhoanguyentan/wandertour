<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('attraction')) {
            Schema::create('attraction', function (Blueprint $table) {
                $table->increments('attr_id');
                $table->string('attr_name')->nullable();
                $table->string('en_attr_name')->nullable();
                $table->string('attr_address')->nullable();
                $table->string('en_attr_address')->nullable();
                $table->float('lat');
                $table->float('long');
                $table->string('opening_time_start')->nullable();
                $table->string('opening_time_end')->nullable();
                $table->string('attr_duration')->nullable();
                $table->string('best_time_start')->nullable();
                $table->string('best_time_end')->nullable();
                $table->decimal('price')->nullable();
                $table->string('detail')->nullable();
                $table->string('en_detail')->nullable();
                $table->boolean('display')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attraction');
    }
};
