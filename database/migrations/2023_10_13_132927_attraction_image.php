<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('attraction_image')) {
            Schema::create('attraction_image', function (Blueprint $table) {
                $table->integer('attr_id')->unsigned();
                $table->integer('img_id')->unsigned();
                $table->foreign('attr_id')->references('attr_id')->on('attraction')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('img_id')->references('img_id')->on('image')->onDelete('cascade')->onUpdate('cascade');
                $table->integer('number');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('attraction_image', function (Blueprint $table) {
            $table->dropForeign('attraction_image_attr_id_foreign');
            $table->dropForeign('attraction_image_img_id_foreign');
        });
        Schema::dropIfExists('attraction_image');
    }
};
