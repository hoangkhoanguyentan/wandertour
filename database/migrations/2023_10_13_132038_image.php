<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('image')) {
            Schema::create('image', function (Blueprint $table) {
                $table->increments('img_id');
                $table->string('link');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tour', function (Blueprint $table) {
            $table->dropForeign('tour_img_id_foreign');
            $table->dropColumn('img_id');
        });
        Schema::dropIfExists('image');
    }
};
